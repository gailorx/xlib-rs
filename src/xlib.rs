#![allow(non_uppercase_consts)]
#![allow(non_camel_case_types)]

pub type XID = ::libc::c_ulong;
pub type Mask = ::libc::c_ulong;
pub type Atom = ::libc::c_ulong;
pub type VisualID = ::libc::c_ulong;
pub type Time = ::libc::c_ulong;
pub type Window = XID;
pub type Drawable = XID;
pub type Font = XID;
pub type Pixmap = XID;
pub type Cursor = XID;
pub type Colormap = XID;
pub type GContext = XID;
pub type KeySym = XID;
pub type KeyCode = ::libc::c_uchar;
pub type XPointer = *mut ::libc::c_char;


/************ DEFINES IN X.h ******************/
pub const ParentRelative: ::libc::c_long = 1; /*background pixmap in CreateWindow
                    and ChangeWindowAttributes */

pub const CopyFromParent: ::libc::c_long = 0; /*border pixmap in CreateWindow
                       and ChangeWindowAttributes
                   special VisualID and special window
                       class passed to CreateWindow */

pub const PointerWindow: ::libc::c_long = 0; /*destination window in SendEvent */
pub const InputFocus: ::libc::c_long = 1; /*destination window in SendEvent */

pub const PointerRoot: ::libc::c_long = 1; /*focus window in SetInputFocus */

pub const AnyPropertyType: ::libc::c_long = 0; /*special Atom, passed to GetProperty */

pub const AnyKey: ::libc::c_long = 0; /*special Key Code, passed to GrabKey */

pub const AnyButton: ::libc::c_long = 0; /*special Button Code, passed to GrabButton */

pub const AllTemporary: ::libc::c_long = 0; /*special Resource ID passed to KillClient */

pub const CurrentTime: ::libc::c_long = 0; /*special Time */

pub const NoSymbol: ::libc::c_long = 0; /*special KeySym */

/***************************************************************** 
 * EVENT DEFINITIONS 
 *****************************************************************/

/* Input Event Masks. Used as event-mask window attribute and as arguments
   to Grab requests.  Not to be confused with event names.  */

pub const NoEventMask: ::libc::c_long = 0;
pub const KeyPressMask: ::libc::c_long = (1<<0);
pub const KeyReleaseMask: ::libc::c_long = (1<<1);
pub const ButtonPressMask: ::libc::c_long = (1<<2);
pub const ButtonReleaseMask: ::libc::c_long = (1<<3); 
pub const EnterWindowMask: ::libc::c_long = (1<<4); 
pub const LeaveWindowMask: ::libc::c_long = (1<<5);
pub const PointerMotionMask: ::libc::c_long = (1<<6);
pub const PointerMotionHintMask: ::libc::c_long = (1<<7); 
pub const Button1MotionMask: ::libc::c_long = (1<<8); 
pub const Button2MotionMask: ::libc::c_long = (1<<9); 
pub const Button3MotionMask: ::libc::c_long = (1<<10);
pub const Button4MotionMask: ::libc::c_long = (1<<11);
pub const Button5MotionMask: ::libc::c_long = (1<<12); 
pub const ButtonMotionMask: ::libc::c_long = (1<<13); 
pub const KeymapStateMask: ::libc::c_long = (1<<14);
pub const ExposureMask: ::libc::c_long = (1<<15);
pub const VisibilityChangeMask: ::libc::c_long = (1<<16);
pub const StructureNotifyMask: ::libc::c_long = (1<<17);
pub const ResizeRedirectMask: ::libc::c_long = (1<<18);
pub const SubstructureNotifyMask: ::libc::c_long = (1<<19);
pub const SubstructureRedirectMask: ::libc::c_long = (1<<20);
pub const FocusChangeMask: ::libc::c_long = (1<<21);
pub const PropertyChangeMask: ::libc::c_long = (1<<22);
pub const ColormapChangeMask: ::libc::c_long = (1<<23);
pub const OwnerGrabButtonMask: ::libc::c_long = (1<<24);

/* Event names.  Used in "type" field in XEvent structures.  Not to be
confused with event masks above.  They start from 2 because 0 and 1
are reserved in the protocol for errors and replies. */

pub const KeyPress: ::libc::c_long = 2;
pub const KeyRelease: ::libc::c_long = 3;
pub const ButtonPress: ::libc::c_long = 4;
pub const ButtonRelease: ::libc::c_long = 5;
pub const MotionNotify: ::libc::c_long = 6;
pub const EnterNotify: ::libc::c_long = 7;
pub const LeaveNotify: ::libc::c_long = 8;
pub const FocusIn: ::libc::c_long = 9;
pub const FocusOut: ::libc::c_long = 10;
pub const KeymapNotify: ::libc::c_long = 11;
pub const Expose: ::libc::c_long = 12;
pub const GraphicsExpose: ::libc::c_long = 13;
pub const NoExpose: ::libc::c_long = 14;
pub const VisibilityNotify: ::libc::c_long = 15;
pub const CreateNotify: ::libc::c_long = 16;
pub const DestroyNotify: ::libc::c_long = 17;
pub const UnmapNotify: ::libc::c_long = 18;
pub const MapNotify: ::libc::c_long = 19;
pub const MapRequest: ::libc::c_long = 20;
pub const ReparentNotify: ::libc::c_long = 21;
pub const ConfigureNotify: ::libc::c_long = 22;
pub const ConfigureRequest: ::libc::c_long = 23;
pub const GravityNotify: ::libc::c_long = 24;
pub const ResizeRequest: ::libc::c_long = 25;
pub const CirculateNotify: ::libc::c_long = 26;
pub const CirculateRequest: ::libc::c_long = 27;
pub const PropertyNotify: ::libc::c_long = 28;
pub const SelectionClear: ::libc::c_long = 29;
pub const SelectionRequest: ::libc::c_long = 30;
pub const SelectionNotify: ::libc::c_long = 31;
pub const ColormapNotify: ::libc::c_long = 32;
pub const ClientMessage: ::libc::c_long = 33;
pub const MappingNotify: ::libc::c_long = 34;
pub const GenericEvent: ::libc::c_long = 35;
pub const LASTEvent: ::libc::c_long = 36;  /* must be bigger than any event # */


/* Key masks. Used as modifiers to GrabButton and GrabKey, results of QueryPointer,
   state in various key-, mouse-, and button-related events. */

pub const ShiftMask: ::libc::c_long = (1<<0);
pub const LockMask: ::libc::c_long = (1<<1);
pub const ControlMask: ::libc::c_long = (1<<2);
pub const Mod1Mask: ::libc::c_long = (1<<3);
pub const Mod2Mask: ::libc::c_long = (1<<4);
pub const Mod3Mask: ::libc::c_long = (1<<5);
pub const Mod4Mask: ::libc::c_long = (1<<6);
pub const Mod5Mask: ::libc::c_long = (1<<7);

/* modifier names.  Used to build a SetModifierMapping request or
   to read a GetModifierMapping request.  These correspond to the
   masks defined above. */
pub const ShiftMapIndex: ::libc::c_long = 0;
pub const LockMapIndex: ::libc::c_long = 1;
pub const ControlMapIndex: ::libc::c_long = 2;
pub const Mod1MapIndex: ::libc::c_long = 3;
pub const Mod2MapIndex: ::libc::c_long = 4;
pub const Mod3MapIndex: ::libc::c_long = 5;
pub const Mod4MapIndex: ::libc::c_long = 6;
pub const Mod5MapIndex: ::libc::c_long = 7;


/* button masks.  Used in same manner as Key masks above. Not to be confused
   with button names below. */

pub const Button1Mask: ::libc::c_long = (1<<8);
pub const Button2Mask: ::libc::c_long = (1<<9);
pub const Button3Mask: ::libc::c_long = (1<<10);
pub const Button4Mask: ::libc::c_long = (1<<11);
pub const Button5Mask: ::libc::c_long = (1<<12);

pub const AnyModifier: ::libc::c_long = (1<<15);  /* used in GrabButton, GrabKey */


/* button names. Used as arguments to GrabButton and as detail in ButtonPress
   and ButtonRelease events.  Not to be confused with button masks above.
   Note that 0 is already defined above as "AnyButton".  */

pub const Button1: ::libc::c_long = 1;
pub const Button2: ::libc::c_long = 2;
pub const Button3: ::libc::c_long = 3;
pub const Button4: ::libc::c_long = 4;
pub const Button5: ::libc::c_long = 5;

/* Notify modes */

pub const NotifyNormal: ::libc::c_long = 0;
pub const NotifyGrab: ::libc::c_long = 1;
pub const NotifyUngrab: ::libc::c_long = 2;
pub const NotifyWhileGrabbed: ::libc::c_long = 3;

pub const NotifyHint: ::libc::c_long = 1;   /* for MotionNotify events */
               
/* Notify detail */

pub const NotifyAncestor: ::libc::c_long = 0;
pub const NotifyVirtual: ::libc::c_long = 1;
pub const NotifyInferior: ::libc::c_long = 2;
pub const NotifyNonlinear: ::libc::c_long = 3;
pub const NotifyNonlinearVirtual: ::libc::c_long = 4;
pub const NotifyPointer: ::libc::c_long = 5;
pub const NotifyPointerRoot: ::libc::c_long = 6;
pub const NotifyDetailNone: ::libc::c_long = 7;

/* Visibility notify */

pub const VisibilityUnobscured: ::libc::c_long = 0;
pub const VisibilityPartiallyObscured: ::libc::c_long = 1;
pub const VisibilityFullyObscured: ::libc::c_long = 2;

/* Circulation request */

pub const PlaceOnTop: ::libc::c_long = 0;
pub const PlaceOnBottom: ::libc::c_long = 1;

/* protocol families */

pub const FamilyInternet: ::libc::c_long = 0 ;  /* IPv4 */
pub const FamilyDECnet: ::libc::c_long = 1;
pub const FamilyChaos: ::libc::c_long = 2;
pub const FamilyInternet6: ::libc::c_long = 6;   /* IPv6 */

/* authentication families not tied to a specific protocol */
pub const FamilyServerInterpreted: ::libc::c_long = 5;

/* Property notification */

pub const PropertyNewValue: ::libc::c_long = 0;
pub const PropertyDelete: ::libc::c_long = 1;

/* Color Map notification */

pub const ColormapUninstalled: ::libc::c_long = 0;
pub const ColormapInstalled: ::libc::c_long = 1;

/* GrabPointer, GrabButton, GrabKeyboard, GrabKey Modes */

pub const GrabModeSync: ::libc::c_long = 0;
pub const GrabModeAsync: ::libc::c_long = 1;

/* GrabPointer, GrabKeyboard reply status */

pub const GrabSuccess: ::libc::c_long = 0;
pub const AlreadyGrabbed: ::libc::c_long = 1;
pub const GrabInvalidTime: ::libc::c_long = 2;
pub const GrabNotViewable: ::libc::c_long = 3;
pub const GrabFrozen: ::libc::c_long = 4;

/* AllowEvents modes */

pub const AsyncPointer: ::libc::c_long = 0;
pub const SyncPointer: ::libc::c_long = 1;
pub const ReplayPointer: ::libc::c_long = 2;
pub const AsyncKeyboard: ::libc::c_long = 3;
pub const SyncKeyboard: ::libc::c_long = 4;
pub const ReplayKeyboard: ::libc::c_long = 5;
pub const AsyncBoth: ::libc::c_long = 6;
pub const SyncBoth: ::libc::c_long = 7;

/* Used in SetInputFocus, GetInputFocus */
// int on purpose!
pub const RevertToNone: ::libc::c_int = 0;
pub const RevertToPointerRoot: ::libc::c_int = PointerRoot as ::libc::c_int;
pub const RevertToParent: ::libc::c_int = 2;

/*****************************************************************
 * ERROR CODES 
 *****************************************************************/

pub const Success: ::libc::c_long = 0;   /* everything's okay */
pub const BadRequest: ::libc::c_long = 1;    /* bad request code */
pub const BadValue: ::libc::c_long = 2;    /* int parameter out of range */
pub const BadWindow: ::libc::c_long = 3;    /* parameter not a Window */
pub const BadPixmap: ::libc::c_long = 4;    /* parameter not a Pixmap */
pub const BadAtom: ::libc::c_long = 5;    /* parameter not an Atom */
pub const BadCursor: ::libc::c_long = 6;    /* parameter not a Cursor */
pub const BadFont: ::libc::c_long = 7;    /* parameter not a Font */
pub const BadMatch: ::libc::c_long = 8;   /* parameter mismatch */
pub const BadDrawable: ::libc::c_long = 9;    /* parameter not a Pixmap or Window */
pub const BadAccess: ::libc::c_long = 10;    /* depending on context:
                 - key/button already grabbed
                 - attempt to free an illegal 
                   cmap entry 
                - attempt to store into a read-only 
                   color map entry.
                - attempt to modify the access control
                   list from other than the local host.
                */
pub const BadAlloc: ::libc::c_long = 11;   /* insufficient resources */
pub const BadColor: ::libc::c_long = 12;    /* no such colormap */
pub const BadGC: ::libc::c_long = 13;    /* parameter not a GC */
pub const BadIDChoice: ::libc::c_long = 14;    /* choice not in range or already used */
pub const BadName: ::libc::c_long = 15;    /* font or color name doesn't exist */
pub const BadLength: ::libc::c_long = 16;    /* Request length incorrect */
pub const BadImplementation: ::libc::c_long = 17;    /* server is defective */

pub const FirstExtensionError: ::libc::c_long = 128;
pub const LastExtensionError: ::libc::c_long = 255;

/*****************************************************************
 * WINDOW DEFINITIONS 
 *****************************************************************/

/* Window classes used by CreateWindow */
/* Note that CopyFromParent is already defined as 0 above */

pub const InputOutput: ::libc::c_long = 1;
pub const InputOnly: ::libc::c_long = 2;

/* Window attributes for CreateWindow and ChangeWindowAttributes */

pub const CWBackPixmap: ::libc::c_long = (1<<0);
pub const CWBackPixel: ::libc::c_long = (1<<1);
pub const CWBorderPixmap: ::libc::c_long = (1<<2);
pub const CWBorderPixel: ::libc::c_long = (1<<3);
pub const CWBitGravity: ::libc::c_long = (1<<4);
pub const CWWinGravity: ::libc::c_long = (1<<5);
pub const CWBackingStore: ::libc::c_long = (1<<6);
pub const CWBackingPlanes: ::libc::c_long = (1<<7);
pub const CWBackingPixel: ::libc::c_long = (1<<8);
pub const CWOverrideRedirect: ::libc::c_long = (1<<9);
pub const CWSaveUnder: ::libc::c_long = (1<<10);
pub const CWEventMask: ::libc::c_long = (1<<11);
pub const CWDontPropagate: ::libc::c_long = (1<<12);
pub const CWColormap: ::libc::c_long = (1<<13);
pub const CWCursor: ::libc::c_long = (1<<14);

/* ConfigureWindow structure */

pub const CWX: ::libc::c_long = (1<<0);
pub const CWY: ::libc::c_long = (1<<1);
pub const CWWidth: ::libc::c_long = (1<<2);
pub const CWHeight: ::libc::c_long = (1<<3);
pub const CWBorderWidth: ::libc::c_long = (1<<4);
pub const CWSibling: ::libc::c_long = (1<<5);
pub const CWStackMode: ::libc::c_long = (1<<6);


/* Bit Gravity */

pub const ForgetGravity: ::libc::c_long = 0;
pub const NorthWestGravity: ::libc::c_long = 1;
pub const NorthGravity: ::libc::c_long = 2;
pub const NorthEastGravity: ::libc::c_long = 3;
pub const WestGravity: ::libc::c_long = 4;
pub const CenterGravity: ::libc::c_long = 5;
pub const EastGravity: ::libc::c_long = 6;
pub const SouthWestGravity: ::libc::c_long = 7;
pub const SouthGravity: ::libc::c_long = 8;
pub const SouthEastGravity: ::libc::c_long = 9;
pub const constGravity: ::libc::c_long = 10;

/* Window gravity + bit gravity above */

pub const UnmapGravity: ::libc::c_long = 0;

/* Used in CreateWindow for backing-store hint */

pub const NotUseful: ::libc::c_long = 0;
pub const WhenMapped: ::libc::c_long = 1;
pub const Always: ::libc::c_long = 2;

/* Used in GetWindowAttributes reply */

pub const IsUnmapped: ::libc::c_long = 0;
pub const IsUnviewable: ::libc::c_long = 1;
pub const IsViewable: ::libc::c_long = 2;

/* Used in ChangeSaveSet */

pub const SetModeInsert: ::libc::c_long = 0;
pub const SetModeDelete: ::libc::c_long = 1;

/* Used in ChangeCloseDownMode */

pub const DestroyAll: ::libc::c_long = 0;
pub const RetainPermanent: ::libc::c_long = 1;
pub const RetainTemporary: ::libc::c_long = 2;

/* Window stacking method (in configureWindow) */

pub const Above: ::libc::c_long = 0;
pub const Below: ::libc::c_long = 1;
pub const TopIf: ::libc::c_long = 2;
pub const BottomIf: ::libc::c_long = 3;
pub const Opposite: ::libc::c_long = 4;

/* Circulation direction */

pub const RaiseLowest: ::libc::c_long = 0;
pub const LowerHighest: ::libc::c_long = 1;

/* Property modes */

pub const PropModeReplace: ::libc::c_long = 0;
pub const PropModePrepend: ::libc::c_long = 1;
pub const PropModeAppend: ::libc::c_long = 2;


/*****************************************************************
 *  IMAGING 
 *****************************************************************/

/* ImageFormat -- PutImage, GetImage */

pub const XYBitmap: ::libc::c_long = 0;   /* depth 1, XYFormat */
pub const XYPixmap: ::libc::c_long = 1;  /* depth == drawable depth */
pub const ZPixmap: ::libc::c_long = 2;   /* depth == drawable depth */

/*****************************************************************
 *  COLOR MAP STUFF 
 *****************************************************************/

/* For CreateColormap */

pub const AllocNone: ::libc::c_long = 0;  /* create map with no entries */
pub const AllocAll: ::libc::c_long = 1;   /* allocate entire map writeable */


/* Flags used in StoreNamedColor, StoreColors */

pub const DoRed: ::libc::c_long = (1<<0);
pub const DoGreen: ::libc::c_long = (1<<1);
pub const DoBlue: ::libc::c_long = (1<<2);

/*****************************************************************
 * CURSOR STUFF
 *****************************************************************/

/* QueryBestSize Class */

pub const CursorShape: ::libc::c_long = 0;  /* largest size that can be displayed */
pub const TileShape: ::libc::c_long = 1;   /* size tiled fastest */
pub const StippleShape: ::libc::c_long = 2;   /* size stippled fastest */

/***************************************************************** 
 * KEYBOARD/POINTER STUFF
 *****************************************************************/

pub const AutoRepeatModeOff: ::libc::c_long = 0;
pub const AutoRepeatModeOn: ::libc::c_long = 1;
pub const AutoRepeatModeDefault: ::libc::c_long = 2;

pub const LedModeOff: ::libc::c_long = 0;
pub const LedModeOn: ::libc::c_long = 1;

/* masks for ChangeKeyboardControl */

pub const KBKeyClickPercent: ::libc::c_long = (1<<0);
pub const KBBellPercent: ::libc::c_long = (1<<1);
pub const KBBellPitch: ::libc::c_long = (1<<2);
pub const KBBellDuration: ::libc::c_long = (1<<3);
pub const KBLed: ::libc::c_long = (1<<4);
pub const KBLedMode: ::libc::c_long = (1<<5);
pub const KBKey: ::libc::c_long = (1<<6);
pub const KBAutoRepeatMode: ::libc::c_long = (1<<7);

pub const MappingSuccess: ::libc::c_long = 0;
pub const MappingBusy: ::libc::c_long = 1;
pub const MappingFailed: ::libc::c_long = 2;

pub const MappingModifier: ::libc::c_long = 0;
pub const MappingKeyboard: ::libc::c_long = 1;
pub const MappingPointer: ::libc::c_long = 2;

/*****************************************************************
 * SCREEN SAVER STUFF 
 *****************************************************************/

pub const DontPreferBlanking: ::libc::c_long = 0;
pub const PreferBlanking: ::libc::c_long = 1;
pub const DefaultBlanking: ::libc::c_long = 2;

pub const DisableScreenSaver: ::libc::c_long = 0;
pub const DisableScreenInterval: ::libc::c_long = 0;

pub const DontAllowExposures: ::libc::c_long = 0;
pub const AllowExposures: ::libc::c_long = 1;
pub const DefaultExposures: ::libc::c_long = 2;

/* for ForceScreenSaver */

pub const ScreenSaverReset: ::libc::c_long = 0;
pub const ScreenSaverActive: ::libc::c_long = 1;

/*****************************************************************
 * HOSTS AND CONNECTIONS
 *****************************************************************/

/* for ChangeHosts */

pub const HostInsert: ::libc::c_long = 0;
pub const HostDelete: ::libc::c_long = 1;

/* for ChangeAccessControl */

pub const EnableAccess: ::libc::c_long = 1;   
pub const DisableAccess: ::libc::c_long = 0;

/* Display classes  used in opening the connection 
 * Note that the constally allocated ones are even numbered and the
 * dynamically changeable ones are odd numbered */

pub const constGray: ::libc::c_long = 0;
pub const GrayScale: ::libc::c_long = 1;
pub const constColor: ::libc::c_long = 2;
pub const PseudoColor: ::libc::c_long = 3;
pub const TrueColor: ::libc::c_long = 4;
pub const DirectColor: ::libc::c_long = 5;


/* Byte order  used in imageByteOrder and bitmapBitOrder */

pub const LSBFirst: ::libc::c_long = 0;
pub const MSBFirst: ::libc::c_long = 1;


/*****************************************************/
/*##########END OF DEFINES IN X.h####################*/
/*****************************************************/


#[repr(C)]
pub struct Struct__XExtData {
    pub number: ::libc::c_int,
    pub next: *mut Struct__XExtData,
    pub free_private: Option<extern "C" fn
                                                (arg1: *mut Struct__XExtData)
                                                -> ::libc::c_int>,
    pub private_data: XPointer,
}

pub type XExtData = Struct__XExtData;

#[repr(C)]
pub struct XExtCodes {
    pub extension: ::libc::c_int,
    pub major_opcode: ::libc::c_int,
    pub first_event: ::libc::c_int,
    pub first_error: ::libc::c_int,
}

#[repr(C)]
pub struct XPixmapFormatValues {
    pub depth: ::libc::c_int,
    pub bits_per_pixel: ::libc::c_int,
    pub scanline_pad: ::libc::c_int,
}

#[repr(C)]
pub struct XGCValues {
    pub function: ::libc::c_int,
    pub plane_mask: ::libc::c_ulong,
    pub foreground: ::libc::c_ulong,
    pub background: ::libc::c_ulong,
    pub line_width: ::libc::c_int,
    pub line_style: ::libc::c_int,
    pub cap_style: ::libc::c_int,
    pub join_style: ::libc::c_int,
    pub fill_style: ::libc::c_int,
    pub fill_rule: ::libc::c_int,
    pub arc_mode: ::libc::c_int,
    pub tile: Pixmap,
    pub stipple: Pixmap,
    pub ts_x_origin: ::libc::c_int,
    pub ts_y_origin: ::libc::c_int,
    pub font: Font,
    pub subwindow_mode: ::libc::c_int,
    pub graphics_exposures: ::libc::c_int,
    pub clip_x_origin: ::libc::c_int,
    pub clip_y_origin: ::libc::c_int,
    pub clip_mask: Pixmap,
    pub dash_offset: ::libc::c_int,
    pub dashes: ::libc::c_char,
}

pub enum Struct__XGC { }

pub type GC = *mut Struct__XGC;

#[repr(C)]
pub struct Visual {
    pub ext_data: *mut XExtData,
    pub visualid: VisualID,
    pub class: ::libc::c_int,
    pub red_mask: ::libc::c_ulong,
    pub green_mask: ::libc::c_ulong,
    pub blue_mask: ::libc::c_ulong,
    pub bits_per_rgb: ::libc::c_int,
    pub map_entries: ::libc::c_int,
}

#[repr(C)]
pub struct Depth {
    pub depth: ::libc::c_int,
    pub nvisuals: ::libc::c_int,
    pub visuals: *mut Visual,
}

pub enum Struct__XDisplay { }

#[repr(C)]
pub struct Screen {
    pub ext_data: *mut XExtData,
    pub display: *mut Struct__XDisplay,
    pub root: Window,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub mwidth: ::libc::c_int,
    pub mheight: ::libc::c_int,
    pub ndepths: ::libc::c_int,
    pub depths: *mut Depth,
    pub root_depth: ::libc::c_int,
    pub root_visual: *mut Visual,
    pub default_gc: GC,
    pub cmap: Colormap,
    pub white_pixel: ::libc::c_ulong,
    pub black_pixel: ::libc::c_ulong,
    pub max_maps: ::libc::c_int,
    pub min_maps: ::libc::c_int,
    pub backing_store: ::libc::c_int,
    pub save_unders: ::libc::c_int,
    pub root_input_mask: ::libc::c_long,
}

#[repr(C)]
pub struct ScreenFormat {
    pub ext_data: *mut XExtData,
    pub depth: ::libc::c_int,
    pub bits_per_pixel: ::libc::c_int,
    pub scanline_pad: ::libc::c_int,
}

#[repr(C)]
pub struct XSetWindowAttributes {
    pub background_pixmap: Pixmap,
    pub background_pixel: ::libc::c_ulong,
    pub border_pixmap: Pixmap,
    pub border_pixel: ::libc::c_ulong,
    pub bit_gravity: ::libc::c_int,
    pub win_gravity: ::libc::c_int,
    pub backing_store: ::libc::c_int,
    pub backing_planes: ::libc::c_ulong,
    pub backing_pixel: ::libc::c_ulong,
    pub save_under: ::libc::c_int,
    pub event_mask: ::libc::c_long,
    pub do_not_propagate_mask: ::libc::c_long,
    pub override_redirect: ::libc::c_int,
    pub colormap: Colormap,
    pub cursor: Cursor,
}

#[repr(C)]
pub struct XWindowAttributes {
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub border_width: ::libc::c_int,
    pub depth: ::libc::c_int,
    pub visual: *mut Visual,
    pub root: Window,
    pub class: ::libc::c_int,
    pub bit_gravity: ::libc::c_int,
    pub win_gravity: ::libc::c_int,
    pub backing_store: ::libc::c_int,
    pub backing_planes: ::libc::c_ulong,
    pub backing_pixel: ::libc::c_ulong,
    pub save_under: ::libc::c_int,
    pub colormap: Colormap,
    pub map_installed: ::libc::c_int,
    pub map_state: ::libc::c_int,
    pub all_event_masks: ::libc::c_long,
    pub your_event_mask: ::libc::c_long,
    pub do_not_propagate_mask: ::libc::c_long,
    pub override_redirect: ::libc::c_int,
    pub screen: *mut Screen,
}

#[repr(C)]
pub struct XHostAddress {
    pub family: ::libc::c_int,
    pub length: ::libc::c_int,
    pub address: *mut ::libc::c_char,
}
#[repr(C)]
pub struct XServerInterpretedAddress {
    pub typelength: ::libc::c_int,
    pub valuelength: ::libc::c_int,
    pub _type: *mut ::libc::c_char,
    pub value: *mut ::libc::c_char,
}

#[repr(C)]
pub struct Struct__XImage {
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub xoffset: ::libc::c_int,
    pub format: ::libc::c_int,
    pub data: *mut ::libc::c_char,
    pub byte_order: ::libc::c_int,
    pub bitmap_unit: ::libc::c_int,
    pub bitmap_bit_order: ::libc::c_int,
    pub bitmap_pad: ::libc::c_int,
    pub depth: ::libc::c_int,
    pub bytes_per_line: ::libc::c_int,
    pub bits_per_pixel: ::libc::c_int,
    pub red_mask: ::libc::c_ulong,
    pub green_mask: ::libc::c_ulong,
    pub blue_mask: ::libc::c_ulong,
    pub obdata: XPointer,
    pub f: Struct_funcs,
}

#[repr(C)]
pub struct Struct_funcs {
    pub create_image: Option<extern "C" fn
                                                (arg1: *mut Struct__XDisplay,
                                                 arg2: *mut Visual,
                                                 arg3: ::libc::c_uint,
                                                 arg4: ::libc::c_int,
                                                 arg5: ::libc::c_int,
                                                 arg6: *mut ::libc::c_char,
                                                 arg7: ::libc::c_uint,
                                                 arg8: ::libc::c_uint,
                                                 arg9: ::libc::c_int,
                                                 arg10: ::libc::c_int)
                                                -> *mut Struct__XImage>,
    pub destroy_image: Option<extern "C" fn
                                                 (arg1: *mut Struct__XImage)
                                                 -> ::libc::c_int>,
    pub get_pixel: Option<extern "C" fn
                                             (arg1: *mut Struct__XImage,
                                              arg2: ::libc::c_int,
                                              arg3: ::libc::c_int)
                                             -> ::libc::c_ulong>,
    pub put_pixel: Option<extern "C" fn
                                             (arg1: *mut Struct__XImage,
                                              arg2: ::libc::c_int,
                                              arg3: ::libc::c_int,
                                              arg4: ::libc::c_ulong)
                                             -> ::libc::c_int>,
    pub sub_image: Option<extern "C" fn
                                             (arg1: *mut Struct__XImage,
                                              arg2: ::libc::c_int,
                                              arg3: ::libc::c_int,
                                              arg4: ::libc::c_uint,
                                              arg5: ::libc::c_uint)
                                             -> *mut Struct__XImage>,
    pub add_pixel: Option<extern "C" fn
                                             (arg1: *mut Struct__XImage,
                                              arg2: ::libc::c_long)
                                             -> ::libc::c_int>,
}

pub type XImage = Struct__XImage;

#[repr(C)]
pub struct XWindowChanges {
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub border_width: ::libc::c_int,
    pub sibling: Window,
    pub stack_mode: ::libc::c_int,
}

#[repr(C)]
pub struct XColor {
    pub pixel: ::libc::c_ulong,
    pub red: ::libc::c_ushort,
    pub green: ::libc::c_ushort,
    pub blue: ::libc::c_ushort,
    pub flags: ::libc::c_char,
    pub pad: ::libc::c_char,
}

#[repr(C)]
pub struct XSegment {
    pub x1: ::libc::c_short,
    pub y1: ::libc::c_short,
    pub x2: ::libc::c_short,
    pub y2: ::libc::c_short,
}

#[repr(C)]
pub struct XPoint {
    pub x: ::libc::c_short,
    pub y: ::libc::c_short,
}

#[repr(C)]
pub struct XRectangle {
    pub x: ::libc::c_short,
    pub y: ::libc::c_short,
    pub width: ::libc::c_ushort,
    pub height: ::libc::c_ushort,
}

#[repr(C)]
pub struct XArc {
    pub x: ::libc::c_short,
    pub y: ::libc::c_short,
    pub width: ::libc::c_ushort,
    pub height: ::libc::c_ushort,
    pub angle1: ::libc::c_short,
    pub angle2: ::libc::c_short,
}

#[repr(C)]
pub struct XKeyboardControl {
    pub key_click_percent: ::libc::c_int,
    pub bell_percent: ::libc::c_int,
    pub bell_pitch: ::libc::c_int,
    pub bell_duration: ::libc::c_int,
    pub led: ::libc::c_int,
    pub led_mode: ::libc::c_int,
    pub key: ::libc::c_int,
    pub auto_repeat_mode: ::libc::c_int,
}

#[repr(C)]
pub struct XKeyboardState {
    pub key_click_percent: ::libc::c_int,
    pub bell_percent: ::libc::c_int,
    pub bell_pitch: ::libc::c_uint,
    pub bell_duration: ::libc::c_uint,
    pub led_mask: ::libc::c_ulong,
    pub global_auto_repeat: ::libc::c_int,
    pub auto_repeats: [::libc::c_char, ..32u],
}

#[repr(C)]
pub struct XTimeCoord {
    pub time: Time,
    pub x: ::libc::c_short,
    pub y: ::libc::c_short,
}

#[repr(C)]
pub struct XModifierKeymap {
    pub max_keypermod: ::libc::c_int,
    pub modifiermap: *mut KeyCode,
}

pub type Display = Struct__XDisplay;

pub enum Struct__XPrivate { }

pub enum Struct__XrmHashBucketRec { }

#[repr(C)]
pub struct Struct_Unnamed1 {
    pub ext_data: *mut XExtData,
    pub private1: *mut Struct__XPrivate,
    pub fd: ::libc::c_int,
    pub private2: ::libc::c_int,
    pub proto_major_version: ::libc::c_int,
    pub proto_minor_version: ::libc::c_int,
    pub vendor: *mut ::libc::c_char,
    pub private3: XID,
    pub private4: XID,
    pub private5: XID,
    pub private6: ::libc::c_int,
    pub resource_alloc: Option<extern "C" fn
                                                  (arg1:
                                                       *mut Struct__XDisplay)
                                                  -> XID>,
    pub byte_order: ::libc::c_int,
    pub bitmap_unit: ::libc::c_int,
    pub bitmap_pad: ::libc::c_int,
    pub bitmap_bit_order: ::libc::c_int,
    pub nformats: ::libc::c_int,
    pub pixmap_format: *mut ScreenFormat,
    pub private8: ::libc::c_int,
    pub release: ::libc::c_int,
    pub private9: *mut Struct__XPrivate,
    pub private10: *mut Struct__XPrivate,
    pub qlen: ::libc::c_int,
    pub last_request_read: ::libc::c_ulong,
    pub request: ::libc::c_ulong,
    pub private11: XPointer,
    pub private12: XPointer,
    pub private13: XPointer,
    pub private14: XPointer,
    pub max_request_size: ::libc::c_uint,
    pub db: *mut Struct__XrmHashBucketRec,
    pub private15: Option<extern "C" fn
                                             (arg1: *mut Struct__XDisplay)
                                             -> ::libc::c_int>,
    pub display_name: *mut ::libc::c_char,
    pub default_screen: ::libc::c_int,
    pub nscreens: ::libc::c_int,
    pub screens: *mut Screen,
    pub motion_buffer: ::libc::c_ulong,
    pub private16: ::libc::c_ulong,
    pub min_keycode: ::libc::c_int,
    pub max_keycode: ::libc::c_int,
    pub private17: XPointer,
    pub private18: XPointer,
    pub private19: ::libc::c_int,
    pub xdefaults: *mut ::libc::c_char,
}

pub type _XPrivDisplay = *mut Struct_Unnamed1;

#[repr(C)]
pub struct XKeyEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub root: Window,
    pub subwindow: Window,
    pub time: Time,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub x_root: ::libc::c_int,
    pub y_root: ::libc::c_int,
    pub state: ::libc::c_uint,
    pub keycode: ::libc::c_uint,
    pub same_screen: ::libc::c_int,
}

pub type XKeyPressedEvent = XKeyEvent;

pub type XKeyReleasedEvent = XKeyEvent;

#[repr(C)]
pub struct XButtonEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub root: Window,
    pub subwindow: Window,
    pub time: Time,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub x_root: ::libc::c_int,
    pub y_root: ::libc::c_int,
    pub state: ::libc::c_uint,
    pub button: ::libc::c_uint,
    pub same_screen: ::libc::c_int,
}

pub type XButtonPressedEvent = XButtonEvent;

pub type XButtonReleasedEvent = XButtonEvent;

#[repr(C)]
pub struct XMotionEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub root: Window,
    pub subwindow: Window,
    pub time: Time,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub x_root: ::libc::c_int,
    pub y_root: ::libc::c_int,
    pub state: ::libc::c_uint,
    pub is_hint: ::libc::c_char,
    pub same_screen: ::libc::c_int,
}

pub type XPointerMovedEvent = XMotionEvent;

#[repr(C)]
pub struct XCrossingEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub root: Window,
    pub subwindow: Window,
    pub time: Time,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub x_root: ::libc::c_int,
    pub y_root: ::libc::c_int,
    pub mode: ::libc::c_int,
    pub detail: ::libc::c_int,
    pub same_screen: ::libc::c_int,
    pub focus: ::libc::c_int,
    pub state: ::libc::c_uint,
}

pub type XEnterWindowEvent = XCrossingEvent;

pub type XLeaveWindowEvent = XCrossingEvent;

#[repr(C)]
pub struct XFocusChangeEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub mode: ::libc::c_int,
    pub detail: ::libc::c_int,
}

pub type XFocusInEvent = XFocusChangeEvent;

pub type XFocusOutEvent = XFocusChangeEvent;

#[repr(C)]
pub struct XKeymapEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub key_vector: [::libc::c_char, ..32u],
}
#[repr(C)]
pub struct XExposeEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub count: ::libc::c_int,
}
#[repr(C)]
pub struct XGraphicsExposeEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub drawable: Drawable,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub count: ::libc::c_int,
    pub major_code: ::libc::c_int,
    pub minor_code: ::libc::c_int,
}
#[repr(C)]
pub struct XNoExposeEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub drawable: Drawable,
    pub major_code: ::libc::c_int,
    pub minor_code: ::libc::c_int,
}
#[repr(C)]
pub struct XVisibilityEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub state: ::libc::c_int,
}
#[repr(C)]
pub struct XCreateWindowEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub parent: Window,
    pub window: Window,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub border_width: ::libc::c_int,
    pub override_redirect: ::libc::c_int,
}
#[repr(C)]
pub struct XDestroyWindowEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub event: Window,
    pub window: Window,
}
#[repr(C)]
pub struct XUnmapEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub event: Window,
    pub window: Window,
    pub from_configure: ::libc::c_int,
}
#[repr(C)]
pub struct XMapEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub event: Window,
    pub window: Window,
    pub override_redirect: ::libc::c_int,
}
#[repr(C)]
pub struct XMapRequestEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub parent: Window,
    pub window: Window,
}
#[repr(C)]
pub struct XReparentEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub event: Window,
    pub window: Window,
    pub parent: Window,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub override_redirect: ::libc::c_int,
}
#[repr(C)]
pub struct XConfigureEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub event: Window,
    pub window: Window,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub border_width: ::libc::c_int,
    pub above: Window,
    pub override_redirect: ::libc::c_int,
}
#[repr(C)]
pub struct XGravityEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub event: Window,
    pub window: Window,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
}
#[repr(C)]
pub struct XResizeRequestEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
}
#[repr(C)]
pub struct XConfigureRequestEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub parent: Window,
    pub window: Window,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub border_width: ::libc::c_int,
    pub above: Window,
    pub detail: ::libc::c_int,
    pub value_mask: ::libc::c_ulong,
}
#[repr(C)]
pub struct XCirculateEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub event: Window,
    pub window: Window,
    pub place: ::libc::c_int,
}
#[repr(C)]
pub struct XCirculateRequestEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub parent: Window,
    pub window: Window,
    pub place: ::libc::c_int,
}
#[repr(C)]
pub struct XPropertyEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub atom: Atom,
    pub time: Time,
    pub state: ::libc::c_int,
}
#[repr(C)]
pub struct XSelectionClearEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub selection: Atom,
    pub time: Time,
}
#[repr(C)]
pub struct XSelectionRequestEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub owner: Window,
    pub requestor: Window,
    pub selection: Atom,
    pub target: Atom,
    pub property: Atom,
    pub time: Time,
}
#[repr(C)]
pub struct XSelectionEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub requestor: Window,
    pub selection: Atom,
    pub target: Atom,
    pub property: Atom,
    pub time: Time,
}
#[repr(C)]
pub struct XColormapEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub colormap: Colormap,
    pub new: ::libc::c_int,
    pub state: ::libc::c_int,
}
#[repr(C)]
pub struct Union_Unnamed2 {
    pub data: [u64, ..5u],
}
impl Union_Unnamed2 {
    pub fn b(&mut self) -> *mut [::libc::c_char, ..20u] {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn s(&mut self) -> *mut [::libc::c_short, ..10u] {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn l(&mut self) -> *mut [::libc::c_long, ..5u] {
        unsafe { ::std::mem::transmute(self) }
    }
}
#[repr(C)]
pub struct XClientMessageEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub message_type: Atom,
    pub format: ::libc::c_int,
    pub data: Union_Unnamed2,
}
#[repr(C)]
pub struct XMappingEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
    pub request: ::libc::c_int,
    pub first_keycode: ::libc::c_int,
    pub count: ::libc::c_int,
}
#[repr(C)]
pub struct XErrorEvent {
    pub _type: ::libc::c_int,
    pub display: *mut Display,
    pub resourceid: XID,
    pub serial: ::libc::c_ulong,
    pub error_code: ::libc::c_uchar,
    pub request_code: ::libc::c_uchar,
    pub minor_code: ::libc::c_uchar,
}
#[repr(C)]
pub struct XAnyEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub window: Window,
}
#[repr(C)]
pub struct XGenericEvent {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub extension: ::libc::c_int,
    pub evtype: ::libc::c_int,
}
#[repr(C)]
pub struct XGenericEventCookie {
    pub _type: ::libc::c_int,
    pub serial: ::libc::c_ulong,
    pub send_event: ::libc::c_int,
    pub display: *mut Display,
    pub extension: ::libc::c_int,
    pub evtype: ::libc::c_int,
    pub cookie: ::libc::c_uint,
    pub data: *mut ::libc::c_void,
}
#[repr(C)]
pub struct Union__XEvent {
    pub data: [u64, ..24u],
}
impl Union__XEvent {
    pub fn _type(&mut self) -> *mut ::libc::c_int {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xany(&mut self) -> *mut XAnyEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xkey(&mut self) -> *mut XKeyEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xbutton(&mut self) -> *mut XButtonEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xmotion(&mut self) -> *mut XMotionEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xcrossing(&mut self) -> *mut XCrossingEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xfocus(&mut self) -> *mut XFocusChangeEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xexpose(&mut self) -> *mut XExposeEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xgraphicsexpose(&mut self) -> *mut XGraphicsExposeEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xnoexpose(&mut self) -> *mut XNoExposeEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xvisibility(&mut self) -> *mut XVisibilityEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xcreatewindow(&mut self) -> *mut XCreateWindowEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xdestroywindow(&mut self) -> *mut XDestroyWindowEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xunmap(&mut self) -> *mut XUnmapEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xmap(&mut self) -> *mut XMapEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xmaprequest(&mut self) -> *mut XMapRequestEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xreparent(&mut self) -> *mut XReparentEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xconfigure(&mut self) -> *mut XConfigureEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xgravity(&mut self) -> *mut XGravityEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xresizerequest(&mut self) -> *mut XResizeRequestEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xconfigurerequest(&mut self) -> *mut XConfigureRequestEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xcirculate(&mut self) -> *mut XCirculateEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xcirculaterequest(&mut self) -> *mut XCirculateRequestEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xproperty(&mut self) -> *mut XPropertyEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xselectionclear(&mut self) -> *mut XSelectionClearEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xselectionrequest(&mut self) -> *mut XSelectionRequestEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xselection(&mut self) -> *mut XSelectionEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xcolormap(&mut self) -> *mut XColormapEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xclient(&mut self) -> *mut XClientMessageEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xmapping(&mut self) -> *mut XMappingEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xerror(&mut self) -> *mut XErrorEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xkeymap(&mut self) -> *mut XKeymapEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xgeneric(&mut self) -> *mut XGenericEvent {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn xcookie(&mut self) -> *mut XGenericEventCookie {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn pad(&mut self) -> *mut [::libc::c_long, ..24u] {
        unsafe { ::std::mem::transmute(self) }
    }
}
pub type XEvent = Union__XEvent;
#[repr(C)]
pub struct XCharStruct {
    pub lbearing: ::libc::c_short,
    pub rbearing: ::libc::c_short,
    pub width: ::libc::c_short,
    pub ascent: ::libc::c_short,
    pub descent: ::libc::c_short,
    pub attributes: ::libc::c_ushort,
}
#[repr(C)]
pub struct XFontProp {
    pub name: Atom,
    pub card32: ::libc::c_ulong,
}
#[repr(C)]
pub struct XFontStruct {
    pub ext_data: *mut XExtData,
    pub fid: Font,
    pub direction: ::libc::c_uint,
    pub min_char_or_byte2: ::libc::c_uint,
    pub max_char_or_byte2: ::libc::c_uint,
    pub min_byte1: ::libc::c_uint,
    pub max_byte1: ::libc::c_uint,
    pub all_chars_exist: ::libc::c_int,
    pub default_char: ::libc::c_uint,
    pub n_properties: ::libc::c_int,
    pub properties: *mut XFontProp,
    pub min_bounds: XCharStruct,
    pub max_bounds: XCharStruct,
    pub per_char: *mut XCharStruct,
    pub ascent: ::libc::c_int,
    pub descent: ::libc::c_int,
}
#[repr(C)]
pub struct XTextItem {
    pub chars: *mut ::libc::c_char,
    pub nchars: ::libc::c_int,
    pub delta: ::libc::c_int,
    pub font: Font,
}
#[repr(C)]
pub struct XChar2b {
    pub byte1: ::libc::c_uchar,
    pub byte2: ::libc::c_uchar,
}
#[repr(C)]
pub struct XTextItem16 {
    pub chars: *mut XChar2b,
    pub nchars: ::libc::c_int,
    pub delta: ::libc::c_int,
    pub font: Font,
}
#[repr(C)]
pub struct XEDataObject {
    pub data: [u64, ..1u],
}
impl XEDataObject {
    pub fn display(&mut self) -> *mut *mut Display {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn gc(&mut self) -> *mut GC { unsafe { ::std::mem::transmute(self) } }
    pub fn visual(&mut self) -> *mut *mut Visual {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn screen(&mut self) -> *mut *mut Screen {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn pixmap_format(&mut self) -> *mut *mut ScreenFormat {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn font(&mut self) -> *mut *mut XFontStruct {
        unsafe { ::std::mem::transmute(self) }
    }
}
#[repr(C)]
pub struct XFontSetExtents {
    pub max_ink_extent: XRectangle,
    pub max_logical_extent: XRectangle,
}
pub enum Struct__XOM { }
pub type XOM = *mut Struct__XOM;
pub enum Struct__XOC { }
pub type XOC = *mut Struct__XOC;
pub type XFontSet = *mut Struct__XOC;
#[repr(C)]
pub struct XmbTextItem {
    pub chars: *mut ::libc::c_char,
    pub nchars: ::libc::c_int,
    pub delta: ::libc::c_int,
    pub font_set: XFontSet,
}
#[repr(C)]
pub struct XwcTextItem {
    pub chars: *mut ::libc::wchar_t,
    pub nchars: ::libc::c_int,
    pub delta: ::libc::c_int,
    pub font_set: XFontSet,
}
#[repr(C)]
pub struct XOMCharSetList {
    pub charset_count: ::libc::c_int,
    pub charset_list: *mut *mut ::libc::c_char,
}
pub type XOrientation = ::libc::c_uint;
pub const XOMOrientation_LTR_TTB: ::libc::c_uint = 0;
pub const XOMOrientation_RTL_TTB: ::libc::c_uint = 1;
pub const XOMOrientation_TTB_LTR: ::libc::c_uint = 2;
pub const XOMOrientation_TTB_RTL: ::libc::c_uint = 3;
pub const XOMOrientation_Context: ::libc::c_uint = 4;
#[repr(C)]
pub struct XOMOrientation {
    pub num_orientation: ::libc::c_int,
    pub orientation: *mut XOrientation,
}
#[repr(C)]
pub struct XOMFontInfo {
    pub num_font: ::libc::c_int,
    pub font_struct_list: *mut *mut XFontStruct,
    pub font_name_list: *mut *mut ::libc::c_char,
}
pub enum Struct__XIM { }
pub type XIM = *mut Struct__XIM;
pub enum Struct__XIC { }
pub type XIC = *mut Struct__XIC;
pub type XIMProc =
    Option<extern "C" fn
                              (arg1: XIM, arg2: XPointer, arg3: XPointer)>;
pub type XICProc =
    Option<extern "C" fn
                              (arg1: XIC, arg2: XPointer, arg3: XPointer)
                              -> ::libc::c_int>;
pub type XIDProc =
    Option<extern "C" fn
                              (arg1: *mut Display, arg2: XPointer,
                               arg3: XPointer)>;
pub type XIMStyle = ::libc::c_ulong;
#[repr(C)]
pub struct XIMStyles {
    pub count_styles: ::libc::c_ushort,
    pub supported_styles: *mut XIMStyle,
}
pub type XVaNestedList = *mut ::libc::c_void;
#[repr(C)]
pub struct XIMCallback {
    pub client_data: XPointer,
    pub callback: XIMProc,
}
#[repr(C)]
pub struct XICCallback {
    pub client_data: XPointer,
    pub callback: XICProc,
}
pub type XIMFeedback = ::libc::c_ulong;
#[repr(C)]
pub struct Struct__XIMText {
    pub length: ::libc::c_ushort,
    pub feedback: *mut XIMFeedback,
    pub encoding_is_wchar: ::libc::c_int,
    pub string: Union_Unnamed3,
}
#[repr(C)]
pub struct Union_Unnamed3 {
    pub data: [u64, ..1u],
}
impl Union_Unnamed3 {
    pub fn multi_byte(&mut self) -> *mut *mut ::libc::c_char {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn wide_char(&mut self) -> *mut *mut ::libc::wchar_t {
        unsafe { ::std::mem::transmute(self) }
    }
}
pub type XIMText = Struct__XIMText;
pub type XIMPreeditState = ::libc::c_ulong;
#[repr(C)]
pub struct Struct__XIMPreeditStateNotifyCallbackStruct {
    pub state: XIMPreeditState,
}
pub type XIMPreeditStateNotifyCallbackStruct =
    Struct__XIMPreeditStateNotifyCallbackStruct;
pub type XIMResetState = ::libc::c_ulong;
pub type XIMStringConversionFeedback = ::libc::c_ulong;
#[repr(C)]
pub struct Struct__XIMStringConversionText {
    pub length: ::libc::c_ushort,
    pub feedback: *mut XIMStringConversionFeedback,
    pub encoding_is_wchar: ::libc::c_int,
    pub string: Union_Unnamed4,
}
#[repr(C)]
pub struct Union_Unnamed4 {
    pub data: [u64, ..1u],
}
impl Union_Unnamed4 {
    pub fn mbs(&mut self) -> *mut *mut ::libc::c_char {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn wcs(&mut self) -> *mut *mut ::libc::wchar_t {
        unsafe { ::std::mem::transmute(self) }
    }
}
pub type XIMStringConversionText = Struct__XIMStringConversionText;
pub type XIMStringConversionPosition = ::libc::c_ushort;
pub type XIMStringConversionType = ::libc::c_ushort;
pub type XIMStringConversionOperation = ::libc::c_ushort;
pub type XIMCaretDirection = ::libc::c_uint;
pub const XIMForwardChar: ::libc::c_uint = 0;
pub const XIMBackwardChar: ::libc::c_uint = 1;
pub const XIMForwardWord: ::libc::c_uint = 2;
pub const XIMBackwardWord: ::libc::c_uint = 3;
pub const XIMCaretUp: ::libc::c_uint = 4;
pub const XIMCaretDown: ::libc::c_uint = 5;
pub const XIMNextLine: ::libc::c_uint = 6;
pub const XIMPreviousLine: ::libc::c_uint = 7;
pub const XIMLineStart: ::libc::c_uint = 8;
pub const XIMLineEnd: ::libc::c_uint = 9;
pub const XIMAbsolutePosition: ::libc::c_uint = 10;
pub const XIMDontChange: ::libc::c_uint = 11;

#[repr(C)]
pub struct Struct__XIMStringConversionCallbackStruct {
    pub position: XIMStringConversionPosition,
    pub direction: XIMCaretDirection,
    pub operation: XIMStringConversionOperation,
    pub factor: ::libc::c_ushort,
    pub text: *mut XIMStringConversionText,
}
pub type XIMStringConversionCallbackStruct =
    Struct__XIMStringConversionCallbackStruct;
#[repr(C)]
pub struct Struct__XIMPreeditDrawCallbackStruct {
    pub caret: ::libc::c_int,
    pub chg_first: ::libc::c_int,
    pub chg_length: ::libc::c_int,
    pub text: *mut XIMText,
}
pub type XIMPreeditDrawCallbackStruct = Struct__XIMPreeditDrawCallbackStruct;
pub type XIMCaretStyle = ::libc::c_uint;
pub const XIMIsInvisible: ::libc::c_uint = 0;
pub const XIMIsPrimary: ::libc::c_uint = 1;
pub const XIMIsSecondary: ::libc::c_uint = 2;
#[repr(C)]
pub struct Struct__XIMPreeditCaretCallbackStruct {
    pub position: ::libc::c_int,
    pub direction: XIMCaretDirection,
    pub style: XIMCaretStyle,
}
pub type XIMPreeditCaretCallbackStruct =
    Struct__XIMPreeditCaretCallbackStruct;
pub type XIMStatusDataType = ::libc::c_uint;
pub const XIMTextType: ::libc::c_uint = 0;
pub const XIMBitmapType: ::libc::c_uint = 1;
#[repr(C)]
pub struct Struct__XIMStatusDrawCallbackStruct {
    pub _type: XIMStatusDataType,
    pub data: Union_Unnamed5,
}
#[repr(C)]
pub struct Union_Unnamed5 {
    pub data: [u64, ..1u],
}
impl Union_Unnamed5 {
    pub fn text(&mut self) -> *mut *mut XIMText {
        unsafe { ::std::mem::transmute(self) }
    }
    pub fn bitmap(&mut self) -> *mut Pixmap {
        unsafe { ::std::mem::transmute(self) }
    }
}
pub type XIMStatusDrawCallbackStruct = Struct__XIMStatusDrawCallbackStruct;
#[repr(C)]
pub struct Struct__XIMHotKeyTrigger {
    pub keysym: KeySym,
    pub modifier: ::libc::c_int,
    pub modifier_mask: ::libc::c_int,
}
pub type XIMHotKeyTrigger = Struct__XIMHotKeyTrigger;
#[repr(C)]
pub struct Struct__XIMHotKeyTriggers {
    pub num_hot_key: ::libc::c_int,
    pub key: *mut XIMHotKeyTrigger,
}
pub type XIMHotKeyTriggers = Struct__XIMHotKeyTriggers;
pub type XIMHotKeyState = ::libc::c_ulong;
#[repr(C)]
pub struct XIMValuesList {
    pub count_values: ::libc::c_ushort,
    pub supported_values: *mut *mut ::libc::c_char,
}
pub type XErrorHandler =
    Option<extern "C" fn
                              (arg1: *mut Display, arg2: *mut XErrorEvent)
                              -> ::libc::c_int>;
pub type XIOErrorHandler =
    Option<extern "C" fn(arg1: *mut Display) -> ::libc::c_int>;
pub type XConnectionWatchProc =
    Option<extern "C" fn
                              (arg1: *mut Display, arg2: XPointer,
                               arg3: ::libc::c_int, arg4: ::libc::c_int,
                               arg5: *mut XPointer)>;
#[repr(C)]
pub struct Struct_Unnamed6 {
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
}
#[repr(C)]
pub struct XSizeHints {
    pub flags: ::libc::c_long,
    pub x: ::libc::c_int,
    pub y: ::libc::c_int,
    pub width: ::libc::c_int,
    pub height: ::libc::c_int,
    pub min_width: ::libc::c_int,
    pub min_height: ::libc::c_int,
    pub max_width: ::libc::c_int,
    pub max_height: ::libc::c_int,
    pub width_inc: ::libc::c_int,
    pub height_inc: ::libc::c_int,
    pub min_aspect: Struct_Unnamed6,
    pub max_aspect: Struct_Unnamed6,
    pub base_width: ::libc::c_int,
    pub base_height: ::libc::c_int,
    pub win_gravity: ::libc::c_int,
}
#[repr(C)]
pub struct XWMHints {
    pub flags: ::libc::c_long,
    pub input: ::libc::c_int,
    pub initial_state: ::libc::c_int,
    pub icon_pixmap: Pixmap,
    pub icon_window: Window,
    pub icon_x: ::libc::c_int,
    pub icon_y: ::libc::c_int,
    pub icon_mask: Pixmap,
    pub window_group: XID,
}
#[repr(C)]
pub struct XTextProperty {
    pub value: *mut ::libc::c_uchar,
    pub encoding: Atom,
    pub format: ::libc::c_int,
    pub nitems: ::libc::c_ulong,
}
pub type XICCEncodingStyle = ::libc::c_uint;
pub const XStringStyle: ::libc::c_uint = 0;
pub const XCompoundTextStyle: ::libc::c_uint = 1;
pub const XTextStyle: ::libc::c_uint = 2;
pub const XICCTextStyle: ::libc::c_uint = 3;
pub const XUTF8StringStyle: ::libc::c_uint = 4;
#[repr(C)]
pub struct XIconSize {
    pub min_width: ::libc::c_int,
    pub min_height: ::libc::c_int,
    pub max_width: ::libc::c_int,
    pub max_height: ::libc::c_int,
    pub width_inc: ::libc::c_int,
    pub height_inc: ::libc::c_int,
}
#[repr(C)]
pub struct XClassHint {
    pub res_name: *mut ::libc::c_char,
    pub res_class: *mut ::libc::c_char,
}
#[repr(C)]
pub struct Struct__XComposeStatus {
    pub compose_ptr: XPointer,
    pub chars_matched: ::libc::c_int,
}
pub type XComposeStatus = Struct__XComposeStatus;
pub enum Struct__XRegion { }
pub type Region = *mut Struct__XRegion;
#[repr(C)]
pub struct XVisualInfo {
    pub visual: *mut Visual,
    pub visualid: VisualID,
    pub screen: ::libc::c_int,
    pub depth: ::libc::c_int,
    pub class: ::libc::c_int,
    pub red_mask: ::libc::c_ulong,
    pub green_mask: ::libc::c_ulong,
    pub blue_mask: ::libc::c_ulong,
    pub colormap_size: ::libc::c_int,
    pub bits_per_rgb: ::libc::c_int,
}
#[repr(C)]
pub struct XStandardColormap {
    pub colormap: Colormap,
    pub red_max: ::libc::c_ulong,
    pub red_mult: ::libc::c_ulong,
    pub green_max: ::libc::c_ulong,
    pub green_mult: ::libc::c_ulong,
    pub blue_max: ::libc::c_ulong,
    pub blue_mult: ::libc::c_ulong,
    pub base_pixel: ::libc::c_ulong,
    pub visualid: VisualID,
    pub killid: XID,
}
pub type XContext = ::libc::c_int;

#[link(name = "libX11.so")]
extern "C" {
    pub static mut _Xdebug: ::libc::c_int;
    pub fn _Xmblen(str: *mut ::libc::c_char, len: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XLoadQueryFont(arg1: *mut Display, arg2: *const ::libc::c_char) ->
     *mut XFontStruct;
    pub fn XQueryFont(arg1: *mut Display, arg2: XID) -> *mut XFontStruct;
    pub fn XGetMotionEvents(arg1: *mut Display, arg2: Window, arg3: Time,
                            arg4: Time, arg5: *mut ::libc::c_int) ->
     *mut XTimeCoord;
    pub fn XDeleteModifiermapEntry(arg1: *mut XModifierKeymap, arg2: KeyCode,
                                   arg3: ::libc::c_int) ->
     *mut XModifierKeymap;
    pub fn XGetModifierMapping(arg1: *mut Display) -> *mut XModifierKeymap;
    pub fn XInsertModifiermapEntry(arg1: *mut XModifierKeymap, arg2: KeyCode,
                                   arg3: ::libc::c_int) ->
     *mut XModifierKeymap;
    pub fn XNewModifiermap(arg1: ::libc::c_int) -> *mut XModifierKeymap;
    pub fn XCreateImage(arg1: *mut Display, arg2: *mut Visual,
                        arg3: ::libc::c_uint, arg4: ::libc::c_int,
                        arg5: ::libc::c_int, arg6: *mut ::libc::c_char,
                        arg7: ::libc::c_uint, arg8: ::libc::c_uint,
                        arg9: ::libc::c_int, arg10: ::libc::c_int) ->
     *mut XImage;
    pub fn XInitImage(arg1: *mut XImage) -> ::libc::c_int;
    pub fn XGetImage(arg1: *mut Display, arg2: Drawable, arg3: ::libc::c_int,
                     arg4: ::libc::c_int, arg5: ::libc::c_uint,
                     arg6: ::libc::c_uint, arg7: ::libc::c_ulong,
                     arg8: ::libc::c_int) -> *mut XImage;
    pub fn XGetSubImage(arg1: *mut Display, arg2: Drawable,
                        arg3: ::libc::c_int, arg4: ::libc::c_int,
                        arg5: ::libc::c_uint, arg6: ::libc::c_uint,
                        arg7: ::libc::c_ulong, arg8: ::libc::c_int,
                        arg9: *mut XImage, arg10: ::libc::c_int,
                        arg11: ::libc::c_int) -> *mut XImage;
    pub fn XOpenDisplay(arg1: *const ::libc::c_char) -> *mut Display;
    pub fn XrmInitialize();
    pub fn XFetchBytes(arg1: *mut Display, arg2: *mut ::libc::c_int) ->
     *mut ::libc::c_char;
    pub fn XFetchBuffer(arg1: *mut Display, arg2: *mut ::libc::c_int,
                        arg3: ::libc::c_int) -> *mut ::libc::c_char;
    pub fn XGetAtomName(arg1: *mut Display, arg2: Atom) ->
     *mut ::libc::c_char;
    pub fn XGetAtomNames(arg1: *mut Display, arg2: *mut Atom,
                         arg3: ::libc::c_int, arg4: *mut *mut ::libc::c_char)
     -> ::libc::c_int;
    pub fn XGetDefault(arg1: *mut Display, arg2: *const ::libc::c_char,
                       arg3: *const ::libc::c_char) -> *mut ::libc::c_char;
    pub fn XDisplayName(arg1: *const ::libc::c_char) -> *mut ::libc::c_char;
    pub fn XKeysymToString(arg1: KeySym) -> *mut ::libc::c_char;
    pub fn XSynchronize(arg1: *mut Display, arg2: ::libc::c_int) ->
     Option<extern "C" fn(arg1: *mut Display)
                               -> ::libc::c_int>;
    pub fn XSetAfterFunction(arg1: *mut Display,
                             arg2:
                                 Option<extern "C" fn
                                                           (arg1:
                                                                *mut Display)
                                                           -> ::libc::c_int>)
     ->
     Option<extern "C" fn(arg1: *mut Display)
                               -> ::libc::c_int>;
    pub fn XInternAtom(arg1: *mut Display, arg2: *const ::libc::c_char,
                       arg3: ::libc::c_int) -> Atom;
    pub fn XInternAtoms(arg1: *mut Display, arg2: *mut *mut ::libc::c_char,
                        arg3: ::libc::c_int, arg4: ::libc::c_int,
                        arg5: *mut Atom) -> ::libc::c_int;
    pub fn XCopyColormapAndFree(arg1: *mut Display, arg2: Colormap) ->
     Colormap;
    pub fn XCreateColormap(arg1: *mut Display, arg2: Window,
                           arg3: *mut Visual, arg4: ::libc::c_int) ->
     Colormap;
    pub fn XCreatePixmapCursor(arg1: *mut Display, arg2: Pixmap, arg3: Pixmap,
                               arg4: *mut XColor, arg5: *mut XColor,
                               arg6: ::libc::c_uint, arg7: ::libc::c_uint) ->
     Cursor;
    pub fn XCreateGlyphCursor(arg1: *mut Display, arg2: Font, arg3: Font,
                              arg4: ::libc::c_uint, arg5: ::libc::c_uint,
                              arg6: *const XColor, arg7: *const XColor) ->
     Cursor;
    pub fn XCreateFontCursor(arg1: *mut Display, arg2: ::libc::c_uint) ->
     Cursor;
    pub fn XLoadFont(arg1: *mut Display, arg2: *const ::libc::c_char) -> Font;
    pub fn XCreateGC(arg1: *mut Display, arg2: Drawable,
                     arg3: ::libc::c_ulong, arg4: *mut XGCValues) -> GC;
    pub fn XGContextFromGC(arg1: GC) -> GContext;
    pub fn XFlushGC(arg1: *mut Display, arg2: GC);
    pub fn XCreatePixmap(arg1: *mut Display, arg2: Drawable,
                         arg3: ::libc::c_uint, arg4: ::libc::c_uint,
                         arg5: ::libc::c_uint) -> Pixmap;
    pub fn XCreateBitmapFromData(arg1: *mut Display, arg2: Drawable,
                                 arg3: *const ::libc::c_char,
                                 arg4: ::libc::c_uint, arg5: ::libc::c_uint)
     -> Pixmap;
    pub fn XCreatePixmapFromBitmapData(arg1: *mut Display, arg2: Drawable,
                                       arg3: *mut ::libc::c_char,
                                       arg4: ::libc::c_uint,
                                       arg5: ::libc::c_uint,
                                       arg6: ::libc::c_ulong,
                                       arg7: ::libc::c_ulong,
                                       arg8: ::libc::c_uint) -> Pixmap;
    pub fn XCreateSimpleWindow(arg1: *mut Display, arg2: Window,
                               arg3: ::libc::c_int, arg4: ::libc::c_int,
                               arg5: ::libc::c_uint, arg6: ::libc::c_uint,
                               arg7: ::libc::c_uint, arg8: ::libc::c_ulong,
                               arg9: ::libc::c_ulong) -> Window;
    pub fn XGetSelectionOwner(arg1: *mut Display, arg2: Atom) -> Window;
    pub fn XCreateWindow(arg1: *mut Display, arg2: Window,
                         arg3: ::libc::c_int, arg4: ::libc::c_int,
                         arg5: ::libc::c_uint, arg6: ::libc::c_uint,
                         arg7: ::libc::c_uint, arg8: ::libc::c_int,
                         arg9: ::libc::c_uint, arg10: *mut Visual,
                         arg11: ::libc::c_ulong,
                         arg12: *mut XSetWindowAttributes) -> Window;
    pub fn XListInstalledColormaps(arg1: *mut Display, arg2: Window,
                                   arg3: *mut ::libc::c_int) -> *mut Colormap;
    pub fn XListFonts(arg1: *mut Display, arg2: *const ::libc::c_char,
                      arg3: ::libc::c_int, arg4: *mut ::libc::c_int) ->
     *mut *mut ::libc::c_char;
    pub fn XListFontsWithInfo(arg1: *mut Display, arg2: *const ::libc::c_char,
                              arg3: ::libc::c_int, arg4: *mut ::libc::c_int,
                              arg5: *mut *mut XFontStruct) ->
     *mut *mut ::libc::c_char;
    pub fn XGetFontPath(arg1: *mut Display, arg2: *mut ::libc::c_int) ->
     *mut *mut ::libc::c_char;
    pub fn XListExtensions(arg1: *mut Display, arg2: *mut ::libc::c_int) ->
     *mut *mut ::libc::c_char;
    pub fn XListProperties(arg1: *mut Display, arg2: Window,
                           arg3: *mut ::libc::c_int) -> *mut Atom;
    pub fn XListHosts(arg1: *mut Display, arg2: *mut ::libc::c_int,
                      arg3: *mut ::libc::c_int) -> *mut XHostAddress;
    pub fn XKeycodeToKeysym(arg1: *mut Display, arg2: KeyCode,
                            arg3: ::libc::c_int) -> KeySym;
    pub fn XLookupKeysym(arg1: *mut XKeyEvent, arg2: ::libc::c_int) -> KeySym;
    pub fn XGetKeyboardMapping(arg1: *mut Display, arg2: KeyCode,
                               arg3: ::libc::c_int, arg4: *mut ::libc::c_int)
     -> *mut KeySym;
    pub fn XStringToKeysym(arg1: *const ::libc::c_char) -> KeySym;
    pub fn XMaxRequestSize(arg1: *mut Display) -> ::libc::c_long;
    pub fn XExtendedMaxRequestSize(arg1: *mut Display) -> ::libc::c_long;
    pub fn XResourceManagerString(arg1: *mut Display) -> *mut ::libc::c_char;
    pub fn XScreenResourceString(arg1: *mut Screen) -> *mut ::libc::c_char;
    pub fn XDisplayMotionBufferSize(arg1: *mut Display) -> ::libc::c_ulong;
    pub fn XVisualIDFromVisual(arg1: *mut Visual) -> VisualID;
    pub fn XInitThreads() -> ::libc::c_int;
    pub fn XLockDisplay(arg1: *mut Display);
    pub fn XUnlockDisplay(arg1: *mut Display);
    pub fn XInitExtension(arg1: *mut Display, arg2: *const ::libc::c_char) ->
     *mut XExtCodes;
    pub fn XAddExtension(arg1: *mut Display) -> *mut XExtCodes;
    pub fn XFindOnExtensionList(arg1: *mut *mut XExtData, arg2: ::libc::c_int)
     -> *mut XExtData;
    pub fn XEHeadOfExtensionList(arg1: XEDataObject) -> *mut *mut XExtData;
    pub fn XRootWindow(arg1: *mut Display, arg2: ::libc::c_int) -> Window;
    pub fn XDefaultRootWindow(arg1: *mut Display) -> Window;
    pub fn XRootWindowOfScreen(arg1: *mut Screen) -> Window;
    pub fn XDefaultVisual(arg1: *mut Display, arg2: ::libc::c_int) ->
     *mut Visual;
    pub fn XDefaultVisualOfScreen(arg1: *mut Screen) -> *mut Visual;
    pub fn XDefaultGC(arg1: *mut Display, arg2: ::libc::c_int) -> GC;
    pub fn XDefaultGCOfScreen(arg1: *mut Screen) -> GC;
    pub fn XBlackPixel(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_ulong;
    pub fn XWhitePixel(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_ulong;
    pub fn XAllPlanes() -> ::libc::c_ulong;
    pub fn XBlackPixelOfScreen(arg1: *mut Screen) -> ::libc::c_ulong;
    pub fn XWhitePixelOfScreen(arg1: *mut Screen) -> ::libc::c_ulong;
    pub fn XNextRequest(arg1: *mut Display) -> ::libc::c_ulong;
    pub fn XLastKnownRequestProcessed(arg1: *mut Display) -> ::libc::c_ulong;
    pub fn XServerVendor(arg1: *mut Display) -> *mut ::libc::c_char;
    pub fn XDisplayString(arg1: *mut Display) -> *mut ::libc::c_char;
    pub fn XDefaultColormap(arg1: *mut Display, arg2: ::libc::c_int) ->
     Colormap;
    pub fn XDefaultColormapOfScreen(arg1: *mut Screen) -> Colormap;
    pub fn XDisplayOfScreen(arg1: *mut Screen) -> *mut Display;
    pub fn XScreenOfDisplay(arg1: *mut Display, arg2: ::libc::c_int) ->
     *mut Screen;
    pub fn XDefaultScreenOfDisplay(arg1: *mut Display) -> *mut Screen;
    pub fn XEventMaskOfScreen(arg1: *mut Screen) -> ::libc::c_long;
    pub fn XScreenNumberOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XSetErrorHandler(arg1: XErrorHandler) -> XErrorHandler;
    pub fn XSetIOErrorHandler(arg1: XIOErrorHandler) -> XIOErrorHandler;
    pub fn XListPixmapFormats(arg1: *mut Display, arg2: *mut ::libc::c_int) ->
     *mut XPixmapFormatValues;
    pub fn XListDepths(arg1: *mut Display, arg2: ::libc::c_int,
                       arg3: *mut ::libc::c_int) -> *mut ::libc::c_int;
    pub fn XReconfigureWMWindow(arg1: *mut Display, arg2: Window,
                                arg3: ::libc::c_int, arg4: ::libc::c_uint,
                                arg5: *mut XWindowChanges) -> ::libc::c_int;
    pub fn XGetWMProtocols(arg1: *mut Display, arg2: Window,
                           arg3: *mut *mut Atom, arg4: *mut ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetWMProtocols(arg1: *mut Display, arg2: Window, arg3: *mut Atom,
                           arg4: ::libc::c_int) -> ::libc::c_int;
    pub fn XIconifyWindow(arg1: *mut Display, arg2: Window,
                          arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XWithdrawWindow(arg1: *mut Display, arg2: Window,
                           arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XGetCommand(arg1: *mut Display, arg2: Window,
                       arg3: *mut *mut *mut ::libc::c_char,
                       arg4: *mut ::libc::c_int) -> ::libc::c_int;
    pub fn XGetWMColormapWindows(arg1: *mut Display, arg2: Window,
                                 arg3: *mut *mut Window,
                                 arg4: *mut ::libc::c_int) -> ::libc::c_int;
    pub fn XSetWMColormapWindows(arg1: *mut Display, arg2: Window,
                                 arg3: *mut Window, arg4: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XFreeStringList(arg1: *mut *mut ::libc::c_char);
    pub fn XSetTransientForHint(arg1: *mut Display, arg2: Window,
                                arg3: Window) -> ::libc::c_int;
    pub fn XActivateScreenSaver(arg1: *mut Display) -> ::libc::c_int;
    pub fn XAddHost(arg1: *mut Display, arg2: *mut XHostAddress) ->
     ::libc::c_int;
    pub fn XAddHosts(arg1: *mut Display, arg2: *mut XHostAddress,
                     arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XAddToExtensionList(arg1: *mut *mut Struct__XExtData,
                               arg2: *mut XExtData) -> ::libc::c_int;
    pub fn XAddToSaveSet(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XAllocColor(arg1: *mut Display, arg2: Colormap, arg3: *mut XColor)
     -> ::libc::c_int;
    pub fn XAllocColorCells(arg1: *mut Display, arg2: Colormap,
                            arg3: ::libc::c_int, arg4: *mut ::libc::c_ulong,
                            arg5: ::libc::c_uint, arg6: *mut ::libc::c_ulong,
                            arg7: ::libc::c_uint) -> ::libc::c_int;
    pub fn XAllocColorPlanes(arg1: *mut Display, arg2: Colormap,
                             arg3: ::libc::c_int, arg4: *mut ::libc::c_ulong,
                             arg5: ::libc::c_int, arg6: ::libc::c_int,
                             arg7: ::libc::c_int, arg8: ::libc::c_int,
                             arg9: *mut ::libc::c_ulong,
                             arg10: *mut ::libc::c_ulong,
                             arg11: *mut ::libc::c_ulong) -> ::libc::c_int;
    pub fn XAllocNamedColor(arg1: *mut Display, arg2: Colormap,
                            arg3: *const ::libc::c_char, arg4: *mut XColor,
                            arg5: *mut XColor) -> ::libc::c_int;
    pub fn XAllowEvents(arg1: *mut Display, arg2: ::libc::c_int, arg3: Time)
     -> ::libc::c_int;
    pub fn XAutoRepeatOff(arg1: *mut Display) -> ::libc::c_int;
    pub fn XAutoRepeatOn(arg1: *mut Display) -> ::libc::c_int;
    pub fn XBell(arg1: *mut Display, arg2: ::libc::c_int) -> ::libc::c_int;
    pub fn XBitmapBitOrder(arg1: *mut Display) -> ::libc::c_int;
    pub fn XBitmapPad(arg1: *mut Display) -> ::libc::c_int;
    pub fn XBitmapUnit(arg1: *mut Display) -> ::libc::c_int;
    pub fn XCellsOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XChangeActivePointerGrab(arg1: *mut Display, arg2: ::libc::c_uint,
                                    arg3: Cursor, arg4: Time) ->
     ::libc::c_int;
    pub fn XChangeGC(arg1: *mut Display, arg2: GC, arg3: ::libc::c_ulong,
                     arg4: *mut XGCValues) -> ::libc::c_int;
    pub fn XChangeKeyboardControl(arg1: *mut Display, arg2: ::libc::c_ulong,
                                  arg3: *mut XKeyboardControl) ->
     ::libc::c_int;
    pub fn XChangeKeyboardMapping(arg1: *mut Display, arg2: ::libc::c_int,
                                  arg3: ::libc::c_int, arg4: *mut KeySym,
                                  arg5: ::libc::c_int) -> ::libc::c_int;
    pub fn XChangePointerControl(arg1: *mut Display, arg2: ::libc::c_int,
                                 arg3: ::libc::c_int, arg4: ::libc::c_int,
                                 arg5: ::libc::c_int, arg6: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XChangeProperty(arg1: *mut Display, arg2: Window, arg3: Atom,
                           arg4: Atom, arg5: ::libc::c_int,
                           arg6: ::libc::c_int, arg7: *const ::libc::c_uchar,
                           arg8: ::libc::c_int) -> ::libc::c_int;
    pub fn XChangeSaveSet(arg1: *mut Display, arg2: Window,
                          arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XChangeWindowAttributes(arg1: *mut Display, arg2: Window,
                                   arg3: ::libc::c_ulong,
                                   arg4: *mut XSetWindowAttributes) ->
     ::libc::c_int;
    pub fn XCheckIfEvent(arg1: *mut Display, arg2: *mut XEvent,
                         arg3:
                             Option<extern "C" fn
                                                       (arg1: *mut Display,
                                                        arg2: *mut XEvent,
                                                        arg3: XPointer)
                                                       -> ::libc::c_int>,
                         arg4: XPointer) -> ::libc::c_int;
    pub fn XCheckMaskEvent(arg1: *mut Display, arg2: ::libc::c_long,
                           arg3: *mut XEvent) -> ::libc::c_int;
    pub fn XCheckTypedEvent(arg1: *mut Display, arg2: ::libc::c_int,
                            arg3: *mut XEvent) -> ::libc::c_int;
    pub fn XCheckTypedWindowEvent(arg1: *mut Display, arg2: Window,
                                  arg3: ::libc::c_int, arg4: *mut XEvent) ->
     ::libc::c_int;
    pub fn XCheckWindowEvent(arg1: *mut Display, arg2: Window,
                             arg3: ::libc::c_long, arg4: *mut XEvent) ->
     ::libc::c_int;
    pub fn XCirculateSubwindows(arg1: *mut Display, arg2: Window,
                                arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XCirculateSubwindowsDown(arg1: *mut Display, arg2: Window) ->
     ::libc::c_int;
    pub fn XCirculateSubwindowsUp(arg1: *mut Display, arg2: Window) ->
     ::libc::c_int;
    pub fn XClearArea(arg1: *mut Display, arg2: Window, arg3: ::libc::c_int,
                      arg4: ::libc::c_int, arg5: ::libc::c_uint,
                      arg6: ::libc::c_uint, arg7: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XClearWindow(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XCloseDisplay(arg1: *mut Display) -> ::libc::c_int;
    pub fn XConfigureWindow(arg1: *mut Display, arg2: Window,
                            arg3: ::libc::c_uint, arg4: *mut XWindowChanges)
     -> ::libc::c_int;
    pub fn XConnectionNumber(arg1: *mut Display) -> ::libc::c_int;
    pub fn XConvertSelection(arg1: *mut Display, arg2: Atom, arg3: Atom,
                             arg4: Atom, arg5: Window, arg6: Time) ->
     ::libc::c_int;
    pub fn XCopyArea(arg1: *mut Display, arg2: Drawable, arg3: Drawable,
                     arg4: GC, arg5: ::libc::c_int, arg6: ::libc::c_int,
                     arg7: ::libc::c_uint, arg8: ::libc::c_uint,
                     arg9: ::libc::c_int, arg10: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XCopyGC(arg1: *mut Display, arg2: GC, arg3: ::libc::c_ulong,
                   arg4: GC) -> ::libc::c_int;
    pub fn XCopyPlane(arg1: *mut Display, arg2: Drawable, arg3: Drawable,
                      arg4: GC, arg5: ::libc::c_int, arg6: ::libc::c_int,
                      arg7: ::libc::c_uint, arg8: ::libc::c_uint,
                      arg9: ::libc::c_int, arg10: ::libc::c_int,
                      arg11: ::libc::c_ulong) -> ::libc::c_int;
    pub fn XDefaultDepth(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDefaultDepthOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XDefaultScreen(arg1: *mut Display) -> ::libc::c_int;
    pub fn XDefineCursor(arg1: *mut Display, arg2: Window, arg3: Cursor) ->
     ::libc::c_int;
    pub fn XDeleteProperty(arg1: *mut Display, arg2: Window, arg3: Atom) ->
     ::libc::c_int;
    pub fn XDestroyWindow(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XDestroySubwindows(arg1: *mut Display, arg2: Window) ->
     ::libc::c_int;
    pub fn XDoesBackingStore(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XDoesSaveUnders(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XDisableAccessControl(arg1: *mut Display) -> ::libc::c_int;
    pub fn XDisplayCells(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDisplayHeight(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDisplayHeightMM(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDisplayKeycodes(arg1: *mut Display, arg2: *mut ::libc::c_int,
                            arg3: *mut ::libc::c_int) -> ::libc::c_int;
    pub fn XDisplayPlanes(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDisplayWidth(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDisplayWidthMM(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawArc(arg1: *mut Display, arg2: Drawable, arg3: GC,
                    arg4: ::libc::c_int, arg5: ::libc::c_int,
                    arg6: ::libc::c_uint, arg7: ::libc::c_uint,
                    arg8: ::libc::c_int, arg9: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawArcs(arg1: *mut Display, arg2: Drawable, arg3: GC,
                     arg4: *mut XArc, arg5: ::libc::c_int) -> ::libc::c_int;
    pub fn XDrawImageString(arg1: *mut Display, arg2: Drawable, arg3: GC,
                            arg4: ::libc::c_int, arg5: ::libc::c_int,
                            arg6: *const ::libc::c_char, arg7: ::libc::c_int)
     -> ::libc::c_int;
    pub fn XDrawImageString16(arg1: *mut Display, arg2: Drawable, arg3: GC,
                              arg4: ::libc::c_int, arg5: ::libc::c_int,
                              arg6: *const XChar2b, arg7: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawLine(arg1: *mut Display, arg2: Drawable, arg3: GC,
                     arg4: ::libc::c_int, arg5: ::libc::c_int,
                     arg6: ::libc::c_int, arg7: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawLines(arg1: *mut Display, arg2: Drawable, arg3: GC,
                      arg4: *mut XPoint, arg5: ::libc::c_int,
                      arg6: ::libc::c_int) -> ::libc::c_int;
    pub fn XDrawPoint(arg1: *mut Display, arg2: Drawable, arg3: GC,
                      arg4: ::libc::c_int, arg5: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawPoints(arg1: *mut Display, arg2: Drawable, arg3: GC,
                       arg4: *mut XPoint, arg5: ::libc::c_int,
                       arg6: ::libc::c_int) -> ::libc::c_int;
    pub fn XDrawRectangle(arg1: *mut Display, arg2: Drawable, arg3: GC,
                          arg4: ::libc::c_int, arg5: ::libc::c_int,
                          arg6: ::libc::c_uint, arg7: ::libc::c_uint) ->
     ::libc::c_int;
    pub fn XDrawRectangles(arg1: *mut Display, arg2: Drawable, arg3: GC,
                           arg4: *mut XRectangle, arg5: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawSegments(arg1: *mut Display, arg2: Drawable, arg3: GC,
                         arg4: *mut XSegment, arg5: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawString(arg1: *mut Display, arg2: Drawable, arg3: GC,
                       arg4: ::libc::c_int, arg5: ::libc::c_int,
                       arg6: *const ::libc::c_char, arg7: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawString16(arg1: *mut Display, arg2: Drawable, arg3: GC,
                         arg4: ::libc::c_int, arg5: ::libc::c_int,
                         arg6: *const XChar2b, arg7: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawText(arg1: *mut Display, arg2: Drawable, arg3: GC,
                     arg4: ::libc::c_int, arg5: ::libc::c_int,
                     arg6: *mut XTextItem, arg7: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XDrawText16(arg1: *mut Display, arg2: Drawable, arg3: GC,
                       arg4: ::libc::c_int, arg5: ::libc::c_int,
                       arg6: *mut XTextItem16, arg7: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XEnableAccessControl(arg1: *mut Display) -> ::libc::c_int;
    pub fn XEventsQueued(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XFetchName(arg1: *mut Display, arg2: Window,
                      arg3: *mut *mut ::libc::c_char) -> ::libc::c_int;
    pub fn XFillArc(arg1: *mut Display, arg2: Drawable, arg3: GC,
                    arg4: ::libc::c_int, arg5: ::libc::c_int,
                    arg6: ::libc::c_uint, arg7: ::libc::c_uint,
                    arg8: ::libc::c_int, arg9: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XFillArcs(arg1: *mut Display, arg2: Drawable, arg3: GC,
                     arg4: *mut XArc, arg5: ::libc::c_int) -> ::libc::c_int;
    pub fn XFillPolygon(arg1: *mut Display, arg2: Drawable, arg3: GC,
                        arg4: *mut XPoint, arg5: ::libc::c_int,
                        arg6: ::libc::c_int, arg7: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XFillRectangle(arg1: *mut Display, arg2: Drawable, arg3: GC,
                          arg4: ::libc::c_int, arg5: ::libc::c_int,
                          arg6: ::libc::c_uint, arg7: ::libc::c_uint) ->
     ::libc::c_int;
    pub fn XFillRectangles(arg1: *mut Display, arg2: Drawable, arg3: GC,
                           arg4: *mut XRectangle, arg5: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XFlush(arg1: *mut Display) -> ::libc::c_int;
    pub fn XForceScreenSaver(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XFree(arg1: *mut ::libc::c_void) -> ::libc::c_int;
    pub fn XFreeColormap(arg1: *mut Display, arg2: Colormap) -> ::libc::c_int;
    pub fn XFreeColors(arg1: *mut Display, arg2: Colormap,
                       arg3: *mut ::libc::c_ulong, arg4: ::libc::c_int,
                       arg5: ::libc::c_ulong) -> ::libc::c_int;
    pub fn XFreeCursor(arg1: *mut Display, arg2: Cursor) -> ::libc::c_int;
    pub fn XFreeExtensionList(arg1: *mut *mut ::libc::c_char) ->
     ::libc::c_int;
    pub fn XFreeFont(arg1: *mut Display, arg2: *mut XFontStruct) ->
     ::libc::c_int;
    pub fn XFreeFontInfo(arg1: *mut *mut ::libc::c_char,
                         arg2: *mut XFontStruct, arg3: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XFreeFontNames(arg1: *mut *mut ::libc::c_char) -> ::libc::c_int;
    pub fn XFreeFontPath(arg1: *mut *mut ::libc::c_char) -> ::libc::c_int;
    pub fn XFreeGC(arg1: *mut Display, arg2: GC) -> ::libc::c_int;
    pub fn XFreeModifiermap(arg1: *mut XModifierKeymap) -> ::libc::c_int;
    pub fn XFreePixmap(arg1: *mut Display, arg2: Pixmap) -> ::libc::c_int;
    pub fn XGeometry(arg1: *mut Display, arg2: ::libc::c_int,
                     arg3: *const ::libc::c_char, arg4: *const ::libc::c_char,
                     arg5: ::libc::c_uint, arg6: ::libc::c_uint,
                     arg7: ::libc::c_uint, arg8: ::libc::c_int,
                     arg9: ::libc::c_int, arg10: *mut ::libc::c_int,
                     arg11: *mut ::libc::c_int, arg12: *mut ::libc::c_int,
                     arg13: *mut ::libc::c_int) -> ::libc::c_int;
    pub fn XGetErrorDatabaseText(arg1: *mut Display,
                                 arg2: *const ::libc::c_char,
                                 arg3: *const ::libc::c_char,
                                 arg4: *const ::libc::c_char,
                                 arg5: *mut ::libc::c_char,
                                 arg6: ::libc::c_int) -> ::libc::c_int;
    pub fn XGetErrorText(arg1: *mut Display, arg2: ::libc::c_int,
                         arg3: *mut ::libc::c_char, arg4: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XGetFontProperty(arg1: *mut XFontStruct, arg2: Atom,
                            arg3: *mut ::libc::c_ulong) -> ::libc::c_int;
    pub fn XGetGCValues(arg1: *mut Display, arg2: GC, arg3: ::libc::c_ulong,
                        arg4: *mut XGCValues) -> ::libc::c_int;
    pub fn XGetGeometry(arg1: *mut Display, arg2: Drawable, arg3: *mut Window,
                        arg4: *mut ::libc::c_int, arg5: *mut ::libc::c_int,
                        arg6: *mut ::libc::c_uint, arg7: *mut ::libc::c_uint,
                        arg8: *mut ::libc::c_uint, arg9: *mut ::libc::c_uint)
     -> ::libc::c_int;
    pub fn XGetIconName(arg1: *mut Display, arg2: Window,
                        arg3: *mut *mut ::libc::c_char) -> ::libc::c_int;
    pub fn XGetInputFocus(arg1: *mut Display, arg2: *mut Window,
                          arg3: *mut ::libc::c_int) -> ::libc::c_int;
    pub fn XGetKeyboardControl(arg1: *mut Display, arg2: *mut XKeyboardState)
     -> ::libc::c_int;
    pub fn XGetPointerControl(arg1: *mut Display, arg2: *mut ::libc::c_int,
                              arg3: *mut ::libc::c_int,
                              arg4: *mut ::libc::c_int) -> ::libc::c_int;
    pub fn XGetPointerMapping(arg1: *mut Display, arg2: *mut ::libc::c_uchar,
                              arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XGetScreenSaver(arg1: *mut Display, arg2: *mut ::libc::c_int,
                           arg3: *mut ::libc::c_int, arg4: *mut ::libc::c_int,
                           arg5: *mut ::libc::c_int) -> ::libc::c_int;
    pub fn XGetTransientForHint(arg1: *mut Display, arg2: Window,
                                arg3: *mut Window) -> ::libc::c_int;
    pub fn XGetWindowProperty(arg1: *mut Display, arg2: Window, arg3: Atom,
                              arg4: ::libc::c_long, arg5: ::libc::c_long,
                              arg6: ::libc::c_int, arg7: Atom,
                              arg8: *mut Atom, arg9: *mut ::libc::c_int,
                              arg10: *mut ::libc::c_ulong,
                              arg11: *mut ::libc::c_ulong,
                              arg12: *mut *mut ::libc::c_uchar) ->
     ::libc::c_int;
    pub fn XGetWindowAttributes(arg1: *mut Display, arg2: Window,
                                arg3: *mut XWindowAttributes) ->
     ::libc::c_int;
    pub fn XGrabButton(arg1: *mut Display, arg2: ::libc::c_uint,
                       arg3: ::libc::c_uint, arg4: Window,
                       arg5: ::libc::c_int, arg6: ::libc::c_uint,
                       arg7: ::libc::c_int, arg8: ::libc::c_int, arg9: Window,
                       arg10: Cursor) -> ::libc::c_int;
    pub fn XGrabKey(arg1: *mut Display, arg2: ::libc::c_int,
                    arg3: ::libc::c_uint, arg4: Window, arg5: ::libc::c_int,
                    arg6: ::libc::c_int, arg7: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XGrabKeyboard(arg1: *mut Display, arg2: Window,
                         arg3: ::libc::c_int, arg4: ::libc::c_int,
                         arg5: ::libc::c_int, arg6: Time) -> ::libc::c_int;
    pub fn XGrabPointer(arg1: *mut Display, arg2: Window, arg3: ::libc::c_int,
                        arg4: ::libc::c_uint, arg5: ::libc::c_int,
                        arg6: ::libc::c_int, arg7: Window, arg8: Cursor,
                        arg9: Time) -> ::libc::c_int;
    pub fn XGrabServer(arg1: *mut Display) -> ::libc::c_int;
    pub fn XHeightMMOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XHeightOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XIfEvent(arg1: *mut Display, arg2: *mut XEvent,
                    arg3:
                        Option<extern "C" fn
                                                  (arg1: *mut Display,
                                                   arg2: *mut XEvent,
                                                   arg3: XPointer)
                                                  -> ::libc::c_int>,
                    arg4: XPointer) -> ::libc::c_int;
    pub fn XImageByteOrder(arg1: *mut Display) -> ::libc::c_int;
    pub fn XInstallColormap(arg1: *mut Display, arg2: Colormap) ->
     ::libc::c_int;
    pub fn XKeysymToKeycode(arg1: *mut Display, arg2: KeySym) -> KeyCode;
    pub fn XKillClient(arg1: *mut Display, arg2: XID) -> ::libc::c_int;
    pub fn XLookupColor(arg1: *mut Display, arg2: Colormap,
                        arg3: *const ::libc::c_char, arg4: *mut XColor,
                        arg5: *mut XColor) -> ::libc::c_int;
    pub fn XLowerWindow(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XMapRaised(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XMapSubwindows(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XMapWindow(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XMaskEvent(arg1: *mut Display, arg2: ::libc::c_long,
                      arg3: *mut XEvent) -> ::libc::c_int;
    pub fn XMaxCmapsOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XMinCmapsOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XMoveResizeWindow(arg1: *mut Display, arg2: Window,
                             arg3: ::libc::c_int, arg4: ::libc::c_int,
                             arg5: ::libc::c_uint, arg6: ::libc::c_uint) ->
     ::libc::c_int;
    pub fn XMoveWindow(arg1: *mut Display, arg2: Window, arg3: ::libc::c_int,
                       arg4: ::libc::c_int) -> ::libc::c_int;
    pub fn XNextEvent(arg1: *mut Display, arg2: *mut XEvent) -> ::libc::c_int;
    pub fn XNoOp(arg1: *mut Display) -> ::libc::c_int;
    pub fn XParseColor(arg1: *mut Display, arg2: Colormap,
                       arg3: *const ::libc::c_char, arg4: *mut XColor) ->
     ::libc::c_int;
    pub fn XParseGeometry(arg1: *const ::libc::c_char,
                          arg2: *mut ::libc::c_int, arg3: *mut ::libc::c_int,
                          arg4: *mut ::libc::c_uint,
                          arg5: *mut ::libc::c_uint) -> ::libc::c_int;
    pub fn XPeekEvent(arg1: *mut Display, arg2: *mut XEvent) -> ::libc::c_int;
    pub fn XPeekIfEvent(arg1: *mut Display, arg2: *mut XEvent,
                        arg3:
                            Option<extern "C" fn
                                                      (arg1: *mut Display,
                                                       arg2: *mut XEvent,
                                                       arg3: XPointer)
                                                      -> ::libc::c_int>,
                        arg4: XPointer) -> ::libc::c_int;
    pub fn XPending(arg1: *mut Display) -> ::libc::c_int;
    pub fn XPlanesOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XProtocolRevision(arg1: *mut Display) -> ::libc::c_int;
    pub fn XProtocolVersion(arg1: *mut Display) -> ::libc::c_int;
    pub fn XPutBackEvent(arg1: *mut Display, arg2: *mut XEvent) ->
     ::libc::c_int;
    pub fn XPutImage(arg1: *mut Display, arg2: Drawable, arg3: GC,
                     arg4: *mut XImage, arg5: ::libc::c_int,
                     arg6: ::libc::c_int, arg7: ::libc::c_int,
                     arg8: ::libc::c_int, arg9: ::libc::c_uint,
                     arg10: ::libc::c_uint) -> ::libc::c_int;
    pub fn XQLength(arg1: *mut Display) -> ::libc::c_int;
    pub fn XQueryBestCursor(arg1: *mut Display, arg2: Drawable,
                            arg3: ::libc::c_uint, arg4: ::libc::c_uint,
                            arg5: *mut ::libc::c_uint,
                            arg6: *mut ::libc::c_uint) -> ::libc::c_int;
    pub fn XQueryBestSize(arg1: *mut Display, arg2: ::libc::c_int,
                          arg3: Drawable, arg4: ::libc::c_uint,
                          arg5: ::libc::c_uint, arg6: *mut ::libc::c_uint,
                          arg7: *mut ::libc::c_uint) -> ::libc::c_int;
    pub fn XQueryBestStipple(arg1: *mut Display, arg2: Drawable,
                             arg3: ::libc::c_uint, arg4: ::libc::c_uint,
                             arg5: *mut ::libc::c_uint,
                             arg6: *mut ::libc::c_uint) -> ::libc::c_int;
    pub fn XQueryBestTile(arg1: *mut Display, arg2: Drawable,
                          arg3: ::libc::c_uint, arg4: ::libc::c_uint,
                          arg5: *mut ::libc::c_uint,
                          arg6: *mut ::libc::c_uint) -> ::libc::c_int;
    pub fn XQueryColor(arg1: *mut Display, arg2: Colormap, arg3: *mut XColor)
     -> ::libc::c_int;
    pub fn XQueryColors(arg1: *mut Display, arg2: Colormap, arg3: *mut XColor,
                        arg4: ::libc::c_int) -> ::libc::c_int;
    pub fn XQueryExtension(arg1: *mut Display, arg2: *const ::libc::c_char,
                           arg3: *mut ::libc::c_int, arg4: *mut ::libc::c_int,
                           arg5: *mut ::libc::c_int) -> ::libc::c_int;
    pub fn XQueryKeymap(arg1: *mut Display, arg2: [::libc::c_char, ..32u]) ->
     ::libc::c_int;
    pub fn XQueryPointer(arg1: *mut Display, arg2: Window, arg3: *mut Window,
                         arg4: *mut Window, arg5: *mut ::libc::c_int,
                         arg6: *mut ::libc::c_int, arg7: *mut ::libc::c_int,
                         arg8: *mut ::libc::c_int, arg9: *mut ::libc::c_uint)
     -> ::libc::c_int;
    pub fn XQueryTextExtents(arg1: *mut Display, arg2: XID,
                             arg3: *const ::libc::c_char, arg4: ::libc::c_int,
                             arg5: *mut ::libc::c_int,
                             arg6: *mut ::libc::c_int,
                             arg7: *mut ::libc::c_int, arg8: *mut XCharStruct)
     -> ::libc::c_int;
    pub fn XQueryTextExtents16(arg1: *mut Display, arg2: XID,
                               arg3: *const XChar2b, arg4: ::libc::c_int,
                               arg5: *mut ::libc::c_int,
                               arg6: *mut ::libc::c_int,
                               arg7: *mut ::libc::c_int,
                               arg8: *mut XCharStruct) -> ::libc::c_int;
    pub fn XQueryTree(arg1: *mut Display, arg2: Window, arg3: *mut Window,
                      arg4: *mut Window, arg5: *mut *mut Window,
                      arg6: *mut ::libc::c_uint) -> ::libc::c_int;
    pub fn XRaiseWindow(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XReadBitmapFile(arg1: *mut Display, arg2: Drawable,
                           arg3: *const ::libc::c_char,
                           arg4: *mut ::libc::c_uint,
                           arg5: *mut ::libc::c_uint, arg6: *mut Pixmap,
                           arg7: *mut ::libc::c_int, arg8: *mut ::libc::c_int)
     -> ::libc::c_int;
    pub fn XReadBitmapFileData(arg1: *const ::libc::c_char,
                               arg2: *mut ::libc::c_uint,
                               arg3: *mut ::libc::c_uint,
                               arg4: *mut *mut ::libc::c_uchar,
                               arg5: *mut ::libc::c_int,
                               arg6: *mut ::libc::c_int) -> ::libc::c_int;
    pub fn XRebindKeysym(arg1: *mut Display, arg2: KeySym, arg3: *mut KeySym,
                         arg4: ::libc::c_int, arg5: *const ::libc::c_uchar,
                         arg6: ::libc::c_int) -> ::libc::c_int;
    pub fn XRecolorCursor(arg1: *mut Display, arg2: Cursor, arg3: *mut XColor,
                          arg4: *mut XColor) -> ::libc::c_int;
    pub fn XRefreshKeyboardMapping(arg1: *mut XMappingEvent) -> ::libc::c_int;
    pub fn XRemoveFromSaveSet(arg1: *mut Display, arg2: Window) ->
     ::libc::c_int;
    pub fn XRemoveHost(arg1: *mut Display, arg2: *mut XHostAddress) ->
     ::libc::c_int;
    pub fn XRemoveHosts(arg1: *mut Display, arg2: *mut XHostAddress,
                        arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XReparentWindow(arg1: *mut Display, arg2: Window, arg3: Window,
                           arg4: ::libc::c_int, arg5: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XResetScreenSaver(arg1: *mut Display) -> ::libc::c_int;
    pub fn XResizeWindow(arg1: *mut Display, arg2: Window,
                         arg3: ::libc::c_uint, arg4: ::libc::c_uint) ->
     ::libc::c_int;
    pub fn XRestackWindows(arg1: *mut Display, arg2: *mut Window,
                           arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XRotateBuffers(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XRotateWindowProperties(arg1: *mut Display, arg2: Window,
                                   arg3: *mut Atom, arg4: ::libc::c_int,
                                   arg5: ::libc::c_int) -> ::libc::c_int;
    pub fn XScreenCount(arg1: *mut Display) -> ::libc::c_int;
    pub fn XSelectInput(arg1: *mut Display, arg2: Window,
                        arg3: ::libc::c_long) -> ::libc::c_int;
    pub fn XSendEvent(arg1: *mut Display, arg2: Window, arg3: ::libc::c_int,
                      arg4: ::libc::c_long, arg5: *mut XEvent) ->
     ::libc::c_int;
    pub fn XSetAccessControl(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetArcMode(arg1: *mut Display, arg2: GC, arg3: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetBackground(arg1: *mut Display, arg2: GC, arg3: ::libc::c_ulong)
     -> ::libc::c_int;
    pub fn XSetClipMask(arg1: *mut Display, arg2: GC, arg3: Pixmap) ->
     ::libc::c_int;
    pub fn XSetClipOrigin(arg1: *mut Display, arg2: GC, arg3: ::libc::c_int,
                          arg4: ::libc::c_int) -> ::libc::c_int;
    pub fn XSetClipRectangles(arg1: *mut Display, arg2: GC,
                              arg3: ::libc::c_int, arg4: ::libc::c_int,
                              arg5: *mut XRectangle, arg6: ::libc::c_int,
                              arg7: ::libc::c_int) -> ::libc::c_int;
    pub fn XSetCloseDownMode(arg1: *mut Display, arg2: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetCommand(arg1: *mut Display, arg2: Window,
                       arg3: *mut *mut ::libc::c_char, arg4: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetDashes(arg1: *mut Display, arg2: GC, arg3: ::libc::c_int,
                      arg4: *const ::libc::c_char, arg5: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetFillRule(arg1: *mut Display, arg2: GC, arg3: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetFillStyle(arg1: *mut Display, arg2: GC, arg3: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetFont(arg1: *mut Display, arg2: GC, arg3: Font) ->
     ::libc::c_int;
    pub fn XSetFontPath(arg1: *mut Display, arg2: *mut *mut ::libc::c_char,
                        arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XSetForeground(arg1: *mut Display, arg2: GC, arg3: ::libc::c_ulong)
     -> ::libc::c_int;
    pub fn XSetFunction(arg1: *mut Display, arg2: GC, arg3: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetGraphicsExposures(arg1: *mut Display, arg2: GC,
                                 arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XSetIconName(arg1: *mut Display, arg2: Window,
                        arg3: *const ::libc::c_char) -> ::libc::c_int;
    pub fn XSetInputFocus(arg1: *mut Display, arg2: Window,
                          arg3: ::libc::c_int, arg4: Time) -> ::libc::c_int;
    pub fn XSetLineAttributes(arg1: *mut Display, arg2: GC,
                              arg3: ::libc::c_uint, arg4: ::libc::c_int,
                              arg5: ::libc::c_int, arg6: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetModifierMapping(arg1: *mut Display, arg2: *mut XModifierKeymap)
     -> ::libc::c_int;
    pub fn XSetPlaneMask(arg1: *mut Display, arg2: GC, arg3: ::libc::c_ulong)
     -> ::libc::c_int;
    pub fn XSetPointerMapping(arg1: *mut Display,
                              arg2: *const ::libc::c_uchar,
                              arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XSetScreenSaver(arg1: *mut Display, arg2: ::libc::c_int,
                           arg3: ::libc::c_int, arg4: ::libc::c_int,
                           arg5: ::libc::c_int) -> ::libc::c_int;
    pub fn XSetSelectionOwner(arg1: *mut Display, arg2: Atom, arg3: Window,
                              arg4: Time) -> ::libc::c_int;
    pub fn XSetState(arg1: *mut Display, arg2: GC, arg3: ::libc::c_ulong,
                     arg4: ::libc::c_ulong, arg5: ::libc::c_int,
                     arg6: ::libc::c_ulong) -> ::libc::c_int;
    pub fn XSetStipple(arg1: *mut Display, arg2: GC, arg3: Pixmap) ->
     ::libc::c_int;
    pub fn XSetSubwindowMode(arg1: *mut Display, arg2: GC,
                             arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XSetTSOrigin(arg1: *mut Display, arg2: GC, arg3: ::libc::c_int,
                        arg4: ::libc::c_int) -> ::libc::c_int;
    pub fn XSetTile(arg1: *mut Display, arg2: GC, arg3: Pixmap) ->
     ::libc::c_int;
    pub fn XSetWindowBackground(arg1: *mut Display, arg2: Window,
                                arg3: ::libc::c_ulong) -> ::libc::c_int;
    pub fn XSetWindowBackgroundPixmap(arg1: *mut Display, arg2: Window,
                                      arg3: Pixmap) -> ::libc::c_int;
    pub fn XSetWindowBorder(arg1: *mut Display, arg2: Window,
                            arg3: ::libc::c_ulong) -> ::libc::c_int;
    pub fn XSetWindowBorderPixmap(arg1: *mut Display, arg2: Window,
                                  arg3: Pixmap) -> ::libc::c_int;
    pub fn XSetWindowBorderWidth(arg1: *mut Display, arg2: Window,
                                 arg3: ::libc::c_uint) -> ::libc::c_int;
    pub fn XSetWindowColormap(arg1: *mut Display, arg2: Window,
                              arg3: Colormap) -> ::libc::c_int;
    pub fn XStoreBuffer(arg1: *mut Display, arg2: *const ::libc::c_char,
                        arg3: ::libc::c_int, arg4: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XStoreBytes(arg1: *mut Display, arg2: *const ::libc::c_char,
                       arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XStoreColor(arg1: *mut Display, arg2: Colormap, arg3: *mut XColor)
     -> ::libc::c_int;
    pub fn XStoreColors(arg1: *mut Display, arg2: Colormap, arg3: *mut XColor,
                        arg4: ::libc::c_int) -> ::libc::c_int;
    pub fn XStoreName(arg1: *mut Display, arg2: Window,
                      arg3: *const ::libc::c_char) -> ::libc::c_int;
    pub fn XStoreNamedColor(arg1: *mut Display, arg2: Colormap,
                            arg3: *const ::libc::c_char,
                            arg4: ::libc::c_ulong, arg5: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSync(arg1: *mut Display, arg2: ::libc::c_int) -> ::libc::c_int;
    pub fn XTextExtents(arg1: *mut XFontStruct, arg2: *const ::libc::c_char,
                        arg3: ::libc::c_int, arg4: *mut ::libc::c_int,
                        arg5: *mut ::libc::c_int, arg6: *mut ::libc::c_int,
                        arg7: *mut XCharStruct) -> ::libc::c_int;
    pub fn XTextExtents16(arg1: *mut XFontStruct, arg2: *const XChar2b,
                          arg3: ::libc::c_int, arg4: *mut ::libc::c_int,
                          arg5: *mut ::libc::c_int, arg6: *mut ::libc::c_int,
                          arg7: *mut XCharStruct) -> ::libc::c_int;
    pub fn XTextWidth(arg1: *mut XFontStruct, arg2: *const ::libc::c_char,
                      arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XTextWidth16(arg1: *mut XFontStruct, arg2: *const XChar2b,
                        arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XTranslateCoordinates(arg1: *mut Display, arg2: Window,
                                 arg3: Window, arg4: ::libc::c_int,
                                 arg5: ::libc::c_int,
                                 arg6: *mut ::libc::c_int,
                                 arg7: *mut ::libc::c_int, arg8: *mut Window)
     -> ::libc::c_int;
    pub fn XUndefineCursor(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XUngrabButton(arg1: *mut Display, arg2: ::libc::c_uint,
                         arg3: ::libc::c_uint, arg4: Window) -> ::libc::c_int;
    pub fn XUngrabKey(arg1: *mut Display, arg2: ::libc::c_int,
                      arg3: ::libc::c_uint, arg4: Window) -> ::libc::c_int;
    pub fn XUngrabKeyboard(arg1: *mut Display, arg2: Time) -> ::libc::c_int;
    pub fn XUngrabPointer(arg1: *mut Display, arg2: Time) -> ::libc::c_int;
    pub fn XUngrabServer(arg1: *mut Display) -> ::libc::c_int;
    pub fn XUninstallColormap(arg1: *mut Display, arg2: Colormap) ->
     ::libc::c_int;
    pub fn XUnloadFont(arg1: *mut Display, arg2: Font) -> ::libc::c_int;
    pub fn XUnmapSubwindows(arg1: *mut Display, arg2: Window) ->
     ::libc::c_int;
    pub fn XUnmapWindow(arg1: *mut Display, arg2: Window) -> ::libc::c_int;
    pub fn XVendorRelease(arg1: *mut Display) -> ::libc::c_int;
    pub fn XWarpPointer(arg1: *mut Display, arg2: Window, arg3: Window,
                        arg4: ::libc::c_int, arg5: ::libc::c_int,
                        arg6: ::libc::c_uint, arg7: ::libc::c_uint,
                        arg8: ::libc::c_int, arg9: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XWidthMMOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XWidthOfScreen(arg1: *mut Screen) -> ::libc::c_int;
    pub fn XWindowEvent(arg1: *mut Display, arg2: Window,
                        arg3: ::libc::c_long, arg4: *mut XEvent) ->
     ::libc::c_int;
    pub fn XWriteBitmapFile(arg1: *mut Display, arg2: *const ::libc::c_char,
                            arg3: Pixmap, arg4: ::libc::c_uint,
                            arg5: ::libc::c_uint, arg6: ::libc::c_int,
                            arg7: ::libc::c_int) -> ::libc::c_int;
    pub fn XSupportsLocale() -> ::libc::c_int;
    pub fn XSetLocaleModifiers(arg1: *const ::libc::c_char) ->
     *mut ::libc::c_char;
    pub fn XOpenOM(arg1: *mut Display, arg2: *mut Struct__XrmHashBucketRec,
                   arg3: *const ::libc::c_char, arg4: *const ::libc::c_char)
     -> XOM;
    pub fn XCloseOM(arg1: XOM) -> ::libc::c_int;
    pub fn XSetOMValues(arg1: XOM, ...) -> *mut ::libc::c_char;
    pub fn XGetOMValues(arg1: XOM, ...) -> *mut ::libc::c_char;
    pub fn XDisplayOfOM(arg1: XOM) -> *mut Display;
    pub fn XLocaleOfOM(arg1: XOM) -> *mut ::libc::c_char;
    pub fn XCreateOC(arg1: XOM, ...) -> XOC;
    pub fn XDestroyOC(arg1: XOC);
    pub fn XOMOfOC(arg1: XOC) -> XOM;
    pub fn XSetOCValues(arg1: XOC, ...) -> *mut ::libc::c_char;
    pub fn XGetOCValues(arg1: XOC, ...) -> *mut ::libc::c_char;
    pub fn XCreateFontSet(arg1: *mut Display, arg2: *const ::libc::c_char,
                          arg3: *mut *mut *mut ::libc::c_char,
                          arg4: *mut ::libc::c_int,
                          arg5: *mut *mut ::libc::c_char) -> XFontSet;
    pub fn XFreeFontSet(arg1: *mut Display, arg2: XFontSet);
    pub fn XFontsOfFontSet(arg1: XFontSet, arg2: *mut *mut *mut XFontStruct,
                           arg3: *mut *mut *mut ::libc::c_char) ->
     ::libc::c_int;
    pub fn XBaseFontNameListOfFontSet(arg1: XFontSet) -> *mut ::libc::c_char;
    pub fn XLocaleOfFontSet(arg1: XFontSet) -> *mut ::libc::c_char;
    pub fn XContextDependentDrawing(arg1: XFontSet) -> ::libc::c_int;
    pub fn XDirectionalDependentDrawing(arg1: XFontSet) -> ::libc::c_int;
    pub fn XContextualDrawing(arg1: XFontSet) -> ::libc::c_int;
    pub fn XExtentsOfFontSet(arg1: XFontSet) -> *mut XFontSetExtents;
    pub fn XmbTextEscapement(arg1: XFontSet, arg2: *const ::libc::c_char,
                             arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XwcTextEscapement(arg1: XFontSet, arg2: *const ::libc::wchar_t,
                             arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn Xutf8TextEscapement(arg1: XFontSet, arg2: *const ::libc::c_char,
                               arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XmbTextExtents(arg1: XFontSet, arg2: *const ::libc::c_char,
                          arg3: ::libc::c_int, arg4: *mut XRectangle,
                          arg5: *mut XRectangle) -> ::libc::c_int;
    pub fn XwcTextExtents(arg1: XFontSet, arg2: *const ::libc::wchar_t,
                          arg3: ::libc::c_int, arg4: *mut XRectangle,
                          arg5: *mut XRectangle) -> ::libc::c_int;
    pub fn Xutf8TextExtents(arg1: XFontSet, arg2: *const ::libc::c_char,
                            arg3: ::libc::c_int, arg4: *mut XRectangle,
                            arg5: *mut XRectangle) -> ::libc::c_int;
    pub fn XmbTextPerCharExtents(arg1: XFontSet, arg2: *const ::libc::c_char,
                                 arg3: ::libc::c_int, arg4: *mut XRectangle,
                                 arg5: *mut XRectangle, arg6: ::libc::c_int,
                                 arg7: *mut ::libc::c_int,
                                 arg8: *mut XRectangle, arg9: *mut XRectangle)
     -> ::libc::c_int;
    pub fn XwcTextPerCharExtents(arg1: XFontSet, arg2: *const ::libc::wchar_t,
                                 arg3: ::libc::c_int, arg4: *mut XRectangle,
                                 arg5: *mut XRectangle, arg6: ::libc::c_int,
                                 arg7: *mut ::libc::c_int,
                                 arg8: *mut XRectangle, arg9: *mut XRectangle)
     -> ::libc::c_int;
    pub fn Xutf8TextPerCharExtents(arg1: XFontSet,
                                   arg2: *const ::libc::c_char,
                                   arg3: ::libc::c_int, arg4: *mut XRectangle,
                                   arg5: *mut XRectangle, arg6: ::libc::c_int,
                                   arg7: *mut ::libc::c_int,
                                   arg8: *mut XRectangle,
                                   arg9: *mut XRectangle) -> ::libc::c_int;
    pub fn XmbDrawText(arg1: *mut Display, arg2: Drawable, arg3: GC,
                       arg4: ::libc::c_int, arg5: ::libc::c_int,
                       arg6: *mut XmbTextItem, arg7: ::libc::c_int);
    pub fn XwcDrawText(arg1: *mut Display, arg2: Drawable, arg3: GC,
                       arg4: ::libc::c_int, arg5: ::libc::c_int,
                       arg6: *mut XwcTextItem, arg7: ::libc::c_int);
    pub fn Xutf8DrawText(arg1: *mut Display, arg2: Drawable, arg3: GC,
                         arg4: ::libc::c_int, arg5: ::libc::c_int,
                         arg6: *mut XmbTextItem, arg7: ::libc::c_int);
    pub fn XmbDrawString(arg1: *mut Display, arg2: Drawable, arg3: XFontSet,
                         arg4: GC, arg5: ::libc::c_int, arg6: ::libc::c_int,
                         arg7: *const ::libc::c_char, arg8: ::libc::c_int);
    pub fn XwcDrawString(arg1: *mut Display, arg2: Drawable, arg3: XFontSet,
                         arg4: GC, arg5: ::libc::c_int, arg6: ::libc::c_int,
                         arg7: *const ::libc::wchar_t, arg8: ::libc::c_int);
    pub fn Xutf8DrawString(arg1: *mut Display, arg2: Drawable, arg3: XFontSet,
                           arg4: GC, arg5: ::libc::c_int, arg6: ::libc::c_int,
                           arg7: *const ::libc::c_char, arg8: ::libc::c_int);
    pub fn XmbDrawImageString(arg1: *mut Display, arg2: Drawable,
                              arg3: XFontSet, arg4: GC, arg5: ::libc::c_int,
                              arg6: ::libc::c_int,
                              arg7: *const ::libc::c_char,
                              arg8: ::libc::c_int);
    pub fn XwcDrawImageString(arg1: *mut Display, arg2: Drawable,
                              arg3: XFontSet, arg4: GC, arg5: ::libc::c_int,
                              arg6: ::libc::c_int, arg7: *const ::libc::wchar_t,
                              arg8: ::libc::c_int);
    pub fn Xutf8DrawImageString(arg1: *mut Display, arg2: Drawable,
                                arg3: XFontSet, arg4: GC, arg5: ::libc::c_int,
                                arg6: ::libc::c_int,
                                arg7: *const ::libc::c_char,
                                arg8: ::libc::c_int);
    pub fn XOpenIM(arg1: *mut Display, arg2: *mut Struct__XrmHashBucketRec,
                   arg3: *mut ::libc::c_char, arg4: *mut ::libc::c_char) ->
     XIM;
    pub fn XCloseIM(arg1: XIM) -> ::libc::c_int;
    pub fn XGetIMValues(arg1: XIM, ...) -> *mut ::libc::c_char;
    pub fn XSetIMValues(arg1: XIM, ...) -> *mut ::libc::c_char;
    pub fn XDisplayOfIM(arg1: XIM) -> *mut Display;
    pub fn XLocaleOfIM(arg1: XIM) -> *mut ::libc::c_char;
    pub fn XCreateIC(arg1: XIM, ...) -> XIC;
    pub fn XDestroyIC(arg1: XIC);
    pub fn XSetICFocus(arg1: XIC);
    pub fn XUnsetICFocus(arg1: XIC);
    pub fn XwcResetIC(arg1: XIC) -> *mut ::libc::wchar_t;
    pub fn XmbResetIC(arg1: XIC) -> *mut ::libc::c_char;
    pub fn Xutf8ResetIC(arg1: XIC) -> *mut ::libc::c_char;
    pub fn XSetICValues(arg1: XIC, ...) -> *mut ::libc::c_char;
    pub fn XGetICValues(arg1: XIC, ...) -> *mut ::libc::c_char;
    pub fn XIMOfIC(arg1: XIC) -> XIM;
    pub fn XFilterEvent(arg1: *mut XEvent, arg2: Window) -> ::libc::c_int;
    pub fn XmbLookupString(arg1: XIC, arg2: *mut XKeyPressedEvent,
                           arg3: *mut ::libc::c_char, arg4: ::libc::c_int,
                           arg5: *mut KeySym, arg6: *mut ::libc::c_int) ->
     ::libc::c_int;
    pub fn XwcLookupString(arg1: XIC, arg2: *mut XKeyPressedEvent,
                           arg3: *mut ::libc::wchar_t, arg4: ::libc::c_int,
                           arg5: *mut KeySym, arg6: *mut ::libc::c_int) ->
     ::libc::c_int;
    pub fn Xutf8LookupString(arg1: XIC, arg2: *mut XKeyPressedEvent,
                             arg3: *mut ::libc::c_char, arg4: ::libc::c_int,
                             arg5: *mut KeySym, arg6: *mut ::libc::c_int) ->
     ::libc::c_int;
    pub fn XVaCreateNestedList(arg1: ::libc::c_int, ...) -> XVaNestedList;
    pub fn XRegisterIMInstantiateCallback(arg1: *mut Display,
                                          arg2: *mut Struct__XrmHashBucketRec,
                                          arg3: *mut ::libc::c_char,
                                          arg4: *mut ::libc::c_char,
                                          arg5: XIDProc, arg6: XPointer) ->
     ::libc::c_int;
    pub fn XUnregisterIMInstantiateCallback(arg1: *mut Display,
                                            arg2:
                                                *mut Struct__XrmHashBucketRec,
                                            arg3: *mut ::libc::c_char,
                                            arg4: *mut ::libc::c_char,
                                            arg5: XIDProc, arg6: XPointer) ->
     ::libc::c_int;
    pub fn XInternalConnectionNumbers(arg1: *mut Display,
                                      arg2: *mut *mut ::libc::c_int,
                                      arg3: *mut ::libc::c_int) ->
     ::libc::c_int;
    pub fn XProcessInternalConnection(arg1: *mut Display,
                                      arg2: ::libc::c_int);
    pub fn XAddConnectionWatch(arg1: *mut Display, arg2: XConnectionWatchProc,
                               arg3: XPointer) -> ::libc::c_int;
    pub fn XRemoveConnectionWatch(arg1: *mut Display,
                                  arg2: XConnectionWatchProc, arg3: XPointer);
    pub fn XSetAuthorization(arg1: *mut ::libc::c_char, arg2: ::libc::c_int,
                             arg3: *mut ::libc::c_char, arg4: ::libc::c_int);
    pub fn _Xmbtowc(arg1: *mut ::libc::wchar_t, arg2: *mut ::libc::c_char,
                    arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn _Xwctomb(arg1: *mut ::libc::c_char, arg2: ::libc::wchar_t) ->
     ::libc::c_int;
    pub fn XGetEventData(arg1: *mut Display, arg2: *mut XGenericEventCookie)
     -> ::libc::c_int;
    pub fn XFreeEventData(arg1: *mut Display, arg2: *mut XGenericEventCookie);
    pub fn XAllocClassHint() -> *mut XClassHint;
    pub fn XAllocIconSize() -> *mut XIconSize;
    pub fn XAllocSizeHints() -> *mut XSizeHints;
    pub fn XAllocStandardColormap() -> *mut XStandardColormap;
    pub fn XAllocWMHints() -> *mut XWMHints;
    pub fn XClipBox(arg1: Region, arg2: *mut XRectangle) -> ::libc::c_int;
    pub fn XCreateRegion() -> Region;
    pub fn XDefaultString() -> *const ::libc::c_char;
    pub fn XDeleteContext(arg1: *mut Display, arg2: XID, arg3: XContext) ->
     ::libc::c_int;
    pub fn XDestroyRegion(arg1: Region) -> ::libc::c_int;
    pub fn XEmptyRegion(arg1: Region) -> ::libc::c_int;
    pub fn XEqualRegion(arg1: Region, arg2: Region) -> ::libc::c_int;
    pub fn XFindContext(arg1: *mut Display, arg2: XID, arg3: XContext,
                        arg4: *mut XPointer) -> ::libc::c_int;
    pub fn XGetClassHint(arg1: *mut Display, arg2: Window,
                         arg3: *mut XClassHint) -> ::libc::c_int;
    pub fn XGetIconSizes(arg1: *mut Display, arg2: Window,
                         arg3: *mut *mut XIconSize, arg4: *mut ::libc::c_int)
     -> ::libc::c_int;
    pub fn XGetNormalHints(arg1: *mut Display, arg2: Window,
                           arg3: *mut XSizeHints) -> ::libc::c_int;
    pub fn XGetRGBColormaps(arg1: *mut Display, arg2: Window,
                            arg3: *mut *mut XStandardColormap,
                            arg4: *mut ::libc::c_int, arg5: Atom) ->
     ::libc::c_int;
    pub fn XGetSizeHints(arg1: *mut Display, arg2: Window,
                         arg3: *mut XSizeHints, arg4: Atom) -> ::libc::c_int;
    pub fn XGetStandardColormap(arg1: *mut Display, arg2: Window,
                                arg3: *mut XStandardColormap, arg4: Atom) ->
     ::libc::c_int;
    pub fn XGetTextProperty(arg1: *mut Display, arg2: Window,
                            arg3: *mut XTextProperty, arg4: Atom) ->
     ::libc::c_int;
    pub fn XGetVisualInfo(arg1: *mut Display, arg2: ::libc::c_long,
                          arg3: *mut XVisualInfo, arg4: *mut ::libc::c_int) ->
     *mut XVisualInfo;
    pub fn XGetWMClientMachine(arg1: *mut Display, arg2: Window,
                               arg3: *mut XTextProperty) -> ::libc::c_int;
    pub fn XGetWMHints(arg1: *mut Display, arg2: Window) -> *mut XWMHints;
    pub fn XGetWMIconName(arg1: *mut Display, arg2: Window,
                          arg3: *mut XTextProperty) -> ::libc::c_int;
    pub fn XGetWMName(arg1: *mut Display, arg2: Window,
                      arg3: *mut XTextProperty) -> ::libc::c_int;
    pub fn XGetWMNormalHints(arg1: *mut Display, arg2: Window,
                             arg3: *mut XSizeHints, arg4: *mut ::libc::c_long)
     -> ::libc::c_int;
    pub fn XGetWMSizeHints(arg1: *mut Display, arg2: Window,
                           arg3: *mut XSizeHints, arg4: *mut ::libc::c_long,
                           arg5: Atom) -> ::libc::c_int;
    pub fn XGetZoomHints(arg1: *mut Display, arg2: Window,
                         arg3: *mut XSizeHints) -> ::libc::c_int;
    pub fn XIntersectRegion(arg1: Region, arg2: Region, arg3: Region) ->
     ::libc::c_int;
    pub fn XConvertCase(arg1: KeySym, arg2: *mut KeySym, arg3: *mut KeySym);
    pub fn XLookupString(arg1: *mut XKeyEvent, arg2: *mut ::libc::c_char,
                         arg3: ::libc::c_int, arg4: *mut KeySym,
                         arg5: *mut XComposeStatus) -> ::libc::c_int;
    pub fn XMatchVisualInfo(arg1: *mut Display, arg2: ::libc::c_int,
                            arg3: ::libc::c_int, arg4: ::libc::c_int,
                            arg5: *mut XVisualInfo) -> ::libc::c_int;
    pub fn XOffsetRegion(arg1: Region, arg2: ::libc::c_int,
                         arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XPointInRegion(arg1: Region, arg2: ::libc::c_int,
                          arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XPolygonRegion(arg1: *mut XPoint, arg2: ::libc::c_int,
                          arg3: ::libc::c_int) -> Region;
    pub fn XRectInRegion(arg1: Region, arg2: ::libc::c_int,
                         arg3: ::libc::c_int, arg4: ::libc::c_uint,
                         arg5: ::libc::c_uint) -> ::libc::c_int;
    pub fn XSaveContext(arg1: *mut Display, arg2: XID, arg3: XContext,
                        arg4: *const ::libc::c_char) -> ::libc::c_int;
    pub fn XSetClassHint(arg1: *mut Display, arg2: Window,
                         arg3: *mut XClassHint) -> ::libc::c_int;
    pub fn XSetIconSizes(arg1: *mut Display, arg2: Window,
                         arg3: *mut XIconSize, arg4: ::libc::c_int) ->
     ::libc::c_int;
    pub fn XSetNormalHints(arg1: *mut Display, arg2: Window,
                           arg3: *mut XSizeHints) -> ::libc::c_int;
    pub fn XSetRGBColormaps(arg1: *mut Display, arg2: Window,
                            arg3: *mut XStandardColormap, arg4: ::libc::c_int,
                            arg5: Atom);
    pub fn XSetSizeHints(arg1: *mut Display, arg2: Window,
                         arg3: *mut XSizeHints, arg4: Atom) -> ::libc::c_int;
    pub fn XSetStandardProperties(arg1: *mut Display, arg2: Window,
                                  arg3: *const ::libc::c_char,
                                  arg4: *const ::libc::c_char, arg5: Pixmap,
                                  arg6: *mut *mut ::libc::c_char,
                                  arg7: ::libc::c_int, arg8: *mut XSizeHints)
     -> ::libc::c_int;
    pub fn XSetTextProperty(arg1: *mut Display, arg2: Window,
                            arg3: *mut XTextProperty, arg4: Atom);
    pub fn XSetWMClientMachine(arg1: *mut Display, arg2: Window,
                               arg3: *mut XTextProperty);
    pub fn XSetWMHints(arg1: *mut Display, arg2: Window, arg3: *mut XWMHints)
     -> ::libc::c_int;
    pub fn XSetWMIconName(arg1: *mut Display, arg2: Window,
                          arg3: *mut XTextProperty);
    pub fn XSetWMName(arg1: *mut Display, arg2: Window,
                      arg3: *mut XTextProperty);
    pub fn XSetWMNormalHints(arg1: *mut Display, arg2: Window,
                             arg3: *mut XSizeHints);
    pub fn XSetWMProperties(arg1: *mut Display, arg2: Window,
                            arg3: *mut XTextProperty,
                            arg4: *mut XTextProperty,
                            arg5: *mut *mut ::libc::c_char,
                            arg6: ::libc::c_int, arg7: *mut XSizeHints,
                            arg8: *mut XWMHints, arg9: *mut XClassHint);
    pub fn XmbSetWMProperties(arg1: *mut Display, arg2: Window,
                              arg3: *const ::libc::c_char,
                              arg4: *const ::libc::c_char,
                              arg5: *mut *mut ::libc::c_char,
                              arg6: ::libc::c_int, arg7: *mut XSizeHints,
                              arg8: *mut XWMHints, arg9: *mut XClassHint);
    pub fn Xutf8SetWMProperties(arg1: *mut Display, arg2: Window,
                                arg3: *const ::libc::c_char,
                                arg4: *const ::libc::c_char,
                                arg5: *mut *mut ::libc::c_char,
                                arg6: ::libc::c_int, arg7: *mut XSizeHints,
                                arg8: *mut XWMHints, arg9: *mut XClassHint);
    pub fn XSetWMSizeHints(arg1: *mut Display, arg2: Window,
                           arg3: *mut XSizeHints, arg4: Atom);
    pub fn XSetRegion(arg1: *mut Display, arg2: GC, arg3: Region) ->
     ::libc::c_int;
    pub fn XSetStandardColormap(arg1: *mut Display, arg2: Window,
                                arg3: *mut XStandardColormap, arg4: Atom);
    pub fn XSetZoomHints(arg1: *mut Display, arg2: Window,
                         arg3: *mut XSizeHints) -> ::libc::c_int;
    pub fn XShrinkRegion(arg1: Region, arg2: ::libc::c_int,
                         arg3: ::libc::c_int) -> ::libc::c_int;
    pub fn XStringListToTextProperty(arg1: *mut *mut ::libc::c_char,
                                     arg2: ::libc::c_int,
                                     arg3: *mut XTextProperty) ->
     ::libc::c_int;
    pub fn XSubtractRegion(arg1: Region, arg2: Region, arg3: Region) ->
     ::libc::c_int;
    pub fn XmbTextListToTextProperty(display: *mut Display,
                                     list: *mut *mut ::libc::c_char,
                                     count: ::libc::c_int,
                                     style: XICCEncodingStyle,
                                     text_prop_return: *mut XTextProperty) ->
     ::libc::c_int;
    pub fn XwcTextListToTextProperty(display: *mut Display,
                                     list: *mut *mut ::libc::wchar_t,
                                     count: ::libc::c_int,
                                     style: XICCEncodingStyle,
                                     text_prop_return: *mut XTextProperty) ->
     ::libc::c_int;
    pub fn Xutf8TextListToTextProperty(display: *mut Display,
                                       list: *mut *mut ::libc::c_char,
                                       count: ::libc::c_int,
                                       style: XICCEncodingStyle,
                                       text_prop_return: *mut XTextProperty)
     -> ::libc::c_int;
    pub fn XwcFreeStringList(list: *mut *mut ::libc::wchar_t);
    pub fn XTextPropertyToStringList(arg1: *mut XTextProperty,
                                     arg2: *mut *mut *mut ::libc::c_char,
                                     arg3: *mut ::libc::c_int) ->
     ::libc::c_int;
    pub fn XmbTextPropertyToTextList(display: *mut Display,
                                     text_prop: *const XTextProperty,
                                     list_return:
                                         *mut *mut *mut ::libc::c_char,
                                     count_return: *mut ::libc::c_int) ->
     ::libc::c_int;
    pub fn XwcTextPropertyToTextList(display: *mut Display,
                                     text_prop: *const XTextProperty,
                                     list_return: *mut *mut *mut ::libc::wchar_t,
                                     count_return: *mut ::libc::c_int) ->
     ::libc::c_int;
    pub fn Xutf8TextPropertyToTextList(display: *mut Display,
                                       text_prop: *const XTextProperty,
                                       list_return:
                                           *mut *mut *mut ::libc::c_char,
                                       count_return: *mut ::libc::c_int) ->
     ::libc::c_int;
    pub fn XUnionRectWithRegion(arg1: *mut XRectangle, arg2: Region,
                                arg3: Region) -> ::libc::c_int;
    pub fn XUnionRegion(arg1: Region, arg2: Region, arg3: Region) ->
     ::libc::c_int;
    pub fn XWMGeometry(arg1: *mut Display, arg2: ::libc::c_int,
                       arg3: *const ::libc::c_char,
                       arg4: *const ::libc::c_char, arg5: ::libc::c_uint,
                       arg6: *mut XSizeHints, arg7: *mut ::libc::c_int,
                       arg8: *mut ::libc::c_int, arg9: *mut ::libc::c_int,
                       arg10: *mut ::libc::c_int, arg11: *mut ::libc::c_int)
     -> ::libc::c_int;
    pub fn XXorRegion(arg1: Region, arg2: Region, arg3: Region) ->
     ::libc::c_int;
}